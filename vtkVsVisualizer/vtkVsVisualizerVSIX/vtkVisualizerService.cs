﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Debugger;
using Microsoft.VisualStudio.Debugger.Evaluation;
using Microsoft.VisualStudio.Debugger.Interop;

namespace vtkVisDbg
{
    /// <summary>
    /// VTK Visualizer service exposed by the package
    /// </summary>
    [Guid("DCD76FC5-E3EB-403C-AD13-B39D70CFA3BC")]
    public interface IVtkVisualizerService
    {
    };

    internal class VtkVisualizerService : IVtkVisualizerService, IVsCppDebugUIVisualizer
    {
        internal class VsDbgExpression
        {     
            /// <summary>
            /// Name of this object, e.g., the name of variable put into the Debug Watch
            /// </summary>
            public string Name { get; private set; }
            
            /// <summary>
            /// Data type, e.g., vtkPolyData*
            /// </summary>
            /// <remarks>
            /// Visual Studio supports specifying module to the data type.
            /// This is useful, if PIMPL mechanism is used, i.e., the public definition 
            /// is a class without any members; (e.g., class vtkInformationVectorInternals;)
            /// and there is some module, where the private implementation is located.
            /// VS needs this module to evaluate the members (otherwise hidden).
            /// Unfortunately, VS 2017 and VS 2019 contains a bug that disallows
            /// evaluation of the Types with modules having '-' or ' ' in its name,
            /// which is typical, however, for VTK.
            /// We, however, expect that we visualize only standard public classes
            /// where the module can be safely stripped off
            /// </remarks>
            public string Type { get; private set; }

            /// <summary>
            /// Expression that can be evaluated.
            /// </summary>
            /// <remarks>
            /// ((Type*)Address)->Member are preferred.
            /// The ctor translates more complex expressions into this form.
            /// </remarks>
            public string Expression { get; private set; }

            /// <summary>
            /// Address into the memory where the data for the object resides
            /// </summary>
            public ulong? Address { get; private set; }

            /// <summary>
            /// Specifies the context of the expression.
            /// </summary>
            /// <remarks>When specified, the full expression for the evaluation is:
            /// {,,modulename}expression
            /// Module name can contain '-' or ' ', so VTK modules are supported.
            /// Unfortunately, the expression then cannot be evaluated, if it 
            /// expands the member whose definition in the module is only a forward
            /// definition (e.g., class vtkAlgorithm) but a proper definition is
            /// in another module because using the context informs the debugger
            /// that identifiers from this module should be used if there is
            /// any ambiguity in identifiers.
            /// </remarks>
            public string CallContext { get; private set; }

            /// <summary>
            /// VS Debugger InspectionContext used to evaluate the expressions.
            /// </summary>
            public DkmInspectionContext InspectionContext { get; private set; }

            public VsDbgExpression(DkmSuccessEvaluationResult evaluationResult,
                DkmInspectionContext inspectionContext, string name = null)
            {
                //the aim is to translate the current DkmSuccessEvaluationResult
                //into something with a condense name (that could be used 
                //in the Visual Debugger to identify our objects),
                //with a simple expression that could be evaluated
                //to get the values of its members
                //and with a pointer to the memory where it resides
                                
                this.Name = name ?? evaluationResult.Name;
                this.Address = evaluationResult.Address?.Value;
                this.Expression = evaluationResult.FullName;
                if (this.Expression.StartsWith("{,,")) //remove call context, if available
                    this.Expression = this.Expression.Substring(this.Expression.IndexOf('}') + 1);

                this.Type = evaluationResult.Type;
                this.InspectionContext = inspectionContext;

                //DkmSuccessEvaluationResult may already contain the valid pointer
                //but this ctor can be called also for related types which results
                //in a reference instead of a pointer, in which case address is null
                if (this.Address == null)
                {
                    //First, the result may be vtkSmartPointer<T>
                    var match = Regex.Match(this.Type, @"vtkSmartPointer\s*<\s*(\w+)\s*>");
                    if (match.Success)
                    {
                        //if it does, we need to extract the object from it
                        this.Type = match.Groups[1].Value;
                        this.Expression = "((" + this.Type + "*)" + this.Expression + ".Object)";
                        this.Address = this.EvaluateExpression(this.Expression).Address?.Value;
                    }
                    else
                    {
                        //Next, the expression is probably a reference to the supported object
                        //This happens when expanding the members in the Debugger Watch
                        //Examples of such expressions are:
                        //*((vtkCommonDataModel-9.0.dll!vtkPolyData*)((vtkPolyData*)poly.Object))->Points
                        //$LinkedListItem((*((vtkCommonCore-9.0.dll!vtkInformationVector*)(*((vtkAlgorithm*)&(*((vtkPolyDataAlgorithm*)&(*((vtkMAFMuscleDecomposition*)&(*((vtkMAFMuscleDecompositionTemplateBased*)&(*((vtkMAFMuscleDecompositionSlicing*)((vtkMAFMuscleDecompositionKukacka*)this->_muscleDecomposers[0].Object))))))))))).Executive->OutputInformation)).Internal->Vector[0]->Internal->Map._List._Mypair._Myval2._Myhead->_Next, 1, _Next)->_Myval.second

                        //The solution is to evaluate the expression "&(original expression)"
                        //however, due to the BUG in VS, the module names with '-' are not supported
                        //we remove them but unfortunately this will render the expressions, in which
                        //private implementations (PIMPL) are accessed, invalid
                        this.Expression = "&(" + Regex.Replace(this.Expression, @"([\w-.]+)!", "") + ")";
                        var exrRes = this.EvaluateExpression(this.Expression);
                        if (exrRes != null)
                        {
                            this.Address = exrRes.Address?.Value;
                            this.Type = exrRes.Type;
                        }
                    }
                }

                //we did all we could to translate the evaluationResult and hopefully the adress 
                //is already available to us
                if (this.Address != null)
                {
                    //Type may contain also module, so we need to remove it
                    //to avoid the BUG in VS (described above)
                    var match = Regex.Match(this.Type, @"!(\w+)");
                    if (match.Success)
                    {
                        this.Type = match.Groups[1].Value;
                    }

                    this.Expression = "((" + this.Type + "*)0x" + this.Address.Value.ToString("X") + ")";

                    //specify also CallContext so we are able to Call Methods
                    //N.B. this cannot be specified earlier as for other expressions this would
                    //hide symbols that might be present in these expressions
                    if (evaluationResult.ExternalModules != null && evaluationResult.ExternalModules.Count > 0)
                    {
                        this.CallContext = "{,," + evaluationResult.ExternalModules[0].Name + "}";
                    }
                }
            }

            /// <summary>
            /// Evaluates the data address of the property being a member of the object described by the current expression.
            /// </summary>            
            /// <returns>An instance of VsDbgExpression, if everything is OK</returns>
            public VsDbgExpression Evaluate(string dataProperty, bool isReferrenced = false)
            {
                var retVal = EvaluateExpression(this.Expression + (isReferrenced ? "." : "->") + dataProperty);
                return retVal != null ? new VsDbgExpression(retVal, this.InspectionContext) : null;
            }


            /// <summary>
            /// Evaluates the cast ((DataType*)0xXXXXXX).
            /// </summary>
            /// <param name="dataType">Required type.</param>
            /// <param name="address">Valid address.</param>
            /// <returns></returns>
            public VsDbgExpression EvaluateCast(string dataType, ulong address)
            {
                var retVal = EvaluateExpression("((" + dataType + "*)0x" + address.ToString("X") + ")");
                return retVal != null ? new VsDbgExpression(retVal, this.InspectionContext) : null;
            }

            /// <summary>
            /// Evaluates the method being a member of the object described by the current expression.
            /// </summary>            
            /// <returns>An instance of VsDbgExpression, if everything is OK</returns>
            public string CallMethod(string methodCall, bool isReferrenced = false)
            {
                var retVal = EvaluateExpression(this.Expression + (isReferrenced ? "." : "->") + methodCall);
                return retVal.Value;
            }
            
            public T CallMethod<T>(string methodCall, bool isReferrenced = false)
            {
                var retVal = CallMethod(methodCall, isReferrenced);
                return (T)Convert.ChangeType(retVal, typeof(T));
            }

            public ulong CallMethodReturningPointer(string methodCall, bool isReferrenced = false)
            {
                var retVal = CallMethod(methodCall, isReferrenced);

                int idxB = retVal.IndexOf('{');
                if (idxB >= 0)
                    retVal = retVal.Substring(0, idxB);

                return Convert.ToUInt64(retVal.Trim(), 16);
            }

            private DkmSuccessEvaluationResult EvaluateExpression(string expression)
            {
                DkmSuccessEvaluationResult retVal = null;
                DkmLanguageExpression langExpr = null;

                try
                {
                    DkmWorkList workList = DkmWorkList.Create(null);

                    // Create the evaluation to add
                    langExpr = DkmLanguageExpression.Create(
                        this.InspectionContext.Language, DkmEvaluationFlags.None,
                        this.CallContext + expression, null);

                    this.InspectionContext.EvaluateExpression(workList, langExpr,
                            this.InspectionContext.Thread.GetTopStackFrame(),
                        delegate (DkmEvaluateExpressionAsyncResult asyncResult)
                        {
                            retVal = asyncResult.ResultObject as DkmSuccessEvaluationResult;
                        });

                    workList.Execute();
                }
                finally
                {
                    if (langExpr != null)
                    {
                        langExpr.Close();
                    }
                }

                return retVal;
            }
        }
                       
        public int DisplayValue(uint ownerHwnd, uint visualizerId, IDebugProperty3 pDebugProperty)
        {
            try
            {
                var dkmEvalResult = DkmSuccessEvaluationResult.ExtractFromProperty(pDebugProperty);

                // Create a new inspection context for all subsequent func-evals in this visualizer
                var inspectionContext = DkmInspectionContext.Create(dkmEvalResult.InspectionSession, 
                    dkmEvalResult.RuntimeInstance, dkmEvalResult.InspectionContext.Thread,
                    1000,   //timeOut
                    DkmEvaluationFlags.None, DkmFuncEvalFlags.None,
                    10,     //format integers in 10 base
                    dkmEvalResult.InspectionContext.Language, null);
                
                //translate the DkmSuccessEvaluationResult into an expression, 
                //in which addresses are used instead of variable names
                var expr = new VsDbgExpression(dkmEvalResult, inspectionContext);
                var polyData = EvaluateExpression(expr);

                //and finally, let us add polyData to visual debugger
                vtkVisualDebuggerVS.AddPolyData(polyData);

                //and visualize it
                vtkVisualDebuggerVS.ShowDialog();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace, "Visualization failed");
                Debug.Fail("Visualization failed: " + e.Message);
                return e.HResult;
            }


            return VSConstants.S_OK;
        }

        private vtkPolyDataVS EvaluateExpression(VsDbgExpression expr)
        {
            vtkPolyDataVS ret = new vtkPolyDataVS(expr.Name);

            switch (expr.Type)
            {
                case "vtkPolyData":
                    ExtractPolyData(expr, ret);
                    break;

                case "vtkPoints":
                    ExtractPoints(expr, ret);
                    break;

                default:
                    return null;
            }

            return ret;
        }

        private void ExtractPolyData(VsDbgExpression expr, vtkPolyDataVS ret)
        {
            //we need to read points first
            ExtractPoints(expr.Evaluate("Points"), ret);

            //now process lines, polys, etc.
            ExtractPolyDataCells(expr.Evaluate("Verts"), ret, vtkPolyDataVSCellType.Verts);
            ExtractPolyDataCells(expr.Evaluate("Lines"), ret, vtkPolyDataVSCellType.Lines);
            ExtractPolyDataCells(expr.Evaluate("Polys"), ret, vtkPolyDataVSCellType.Polys);
            ExtractPolyDataCells(expr.Evaluate("Strips"), ret, vtkPolyDataVSCellType.Strips);

            //finally, extract points and cells data
            //TODO: points and cells data
        }        

        private void ExtractPolyDataCells(VsDbgExpression expr, vtkPolyDataVS ret, vtkPolyDataVSCellType kind)
        {
            if (expr.Address.GetValueOrDefault() == 0)
                return; //no cells available

            //we will need to call several methods to determine size of the data            
            VsDbgExpression IaExpr = expr.Evaluate("Ia");      
            if (IaExpr == null)
            {
                //Ia does not exist => it might be VTK 9.0+
                ExtractPolyDataCells_V2(expr, ret, kind);
                return;
            }

            //Ia exists => VTK prior 9.0                        
            //Assuming Topology:
            //
            //Cell 0: Triangle | point ids: { 0, 1, 2}
            //           *Cell 1: Triangle | point ids: { 5, 7, 2}
            //           *Cell 2: Quad | point ids: { 3, 4, 6, 7}
            //           *Cell 4: Line | point ids: { 5,8}
            //
            //Cells are stored in one dimensional array:
            // Connectivity: {3, 0, 1, 2, 3, 5, 7, 2,     4, 3, 4, 6, 7,   2, 5, 8}
            //               | --Cell 0-- || --Cell 1-- || ----Cell 2-- -|| --C3 -|                            

            ulong address = expr.CallMethodReturningPointer("GetPointer()");
            int nCells = expr.CallMethod<int>("GetNumberOfCells()");        //number of cells
            int size = IaExpr.CallMethod<int>("GetMaxId()") + 1;            //number of the entries in the cell array buffer
            int debugeeElemSizeInBytes = IaExpr.CallMethod<int>("GetDataTypeSize()");
            
            //read the memory from the debugee process to our process
            byte[] debugeeData = new byte[debugeeElemSizeInBytes*size];
            
            DkmProcess process = expr.InspectionContext.Thread.Process;
            process.ReadMemory(address, DkmReadMemoryFlags.None, debugeeData);


            unsafe
            {
                int elemSizeInBytes;
                void* connectivity;
                void* offsets;

                //allocate the cells in the debugger address space
                ret.AllocateCells(nCells, size - nCells, kind, &elemSizeInBytes, &connectivity, &offsets);

                fixed (byte* debugeePtr = debugeeData)
                {
                    if (elemSizeInBytes == sizeof(int))
                    {
                        if (debugeeElemSizeInBytes == sizeof(int))
                            vtkPolyDataVS.LegacyCells_To_V2((int*)debugeePtr, size, (int*)connectivity, (int*)offsets);
                        else if (debugeeElemSizeInBytes == sizeof(long))
                            vtkPolyDataVS.LegacyCells_To_V2((long*)debugeePtr, size, (int*)connectivity, (int*)offsets);
                    }
                    else if (elemSizeInBytes == sizeof(long))
                    {
                        if (debugeeElemSizeInBytes == sizeof(int))
                            vtkPolyDataVS.LegacyCells_To_V2((int*)debugeePtr, size, (long*)connectivity, (long*)offsets);
                        else if (debugeeElemSizeInBytes == sizeof(long))
                            vtkPolyDataVS.LegacyCells_To_V2((long*)debugeePtr, size, (long*)connectivity, (long*)offsets);
                    }
                    else
                        Debug.Fail("Unsupported sizeof(vtkIdType).");
                }                         
            } //end unsafe
        }

        private void ExtractPolyDataCells_V2(VsDbgExpression expr, vtkPolyDataVS ret, vtkPolyDataVSCellType kind)
        {
            //VTK 9.0+
            //Cells are stored in two different
            bool debugee64bit = expr.CallMethod<bool>("IsStorage64Bit()");
            int debugeeElemSizeInBytes = debugee64bit ? sizeof(long) : sizeof(int);

            //get the address of offsets data
            var offsDAExpr = debugee64bit ? expr.EvaluateCast("vtkTypeInt64Array", expr.CallMethodReturningPointer("GetOffsetsArray64()")) :
                expr.EvaluateCast("vtkTypeInt32Array", expr.CallMethodReturningPointer("GetOffsetsArray32()"));

            //get the address of connectivity data
            var connDAExpr = debugee64bit ? expr.EvaluateCast("vtkTypeInt64Array", expr.CallMethodReturningPointer("GetConnectivityArray64()")) :
                expr.EvaluateCast("vtkTypeInt32Array", expr.CallMethodReturningPointer("GetConnectivityArray32()"));

            int nCells = expr.CallMethod<int>("GetNumberOfCells()");                //number of cells
            int nCnnSize = expr.CallMethod<int>("GetNumberOfConnectivityIds()");    //number of entries in connectivity
            
            ulong debugeeOffsetsAddr = offsDAExpr.CallMethodReturningPointer("GetVoidPointer(0)");
            ulong debugeeConnectivityAddr = connDAExpr.CallMethodReturningPointer("GetVoidPointer(0)");

             DkmProcess process = expr.InspectionContext.Thread.Process;

            unsafe
            {
                int elemSizeInBytes;
                void* connectivity;
                void* offsets;

                //allocate the cells in the debugger address space
                ret.AllocateCells(nCells, nCnnSize, kind, &elemSizeInBytes, &connectivity, &offsets);
                if (debugeeElemSizeInBytes == elemSizeInBytes)
                {
                    //direct reading                    
                    process.ReadMemory(debugeeOffsetsAddr, DkmReadMemoryFlags.None, offsets, (nCells + 1)* elemSizeInBytes);
                    process.ReadMemory(debugeeConnectivityAddr, DkmReadMemoryFlags.None, connectivity, nCnnSize * elemSizeInBytes);
                }
                else
                {
                    //read the memory from the debugee process to our process
                    byte[] debugeeOffsetsData = new byte[(nCells + 1)* debugeeElemSizeInBytes];
                    byte[] debugeeConnectivitysData = new byte[nCnnSize * debugeeElemSizeInBytes];

                    process.ReadMemory(debugeeOffsetsAddr, DkmReadMemoryFlags.None, debugeeOffsetsData);
                    process.ReadMemory(debugeeConnectivityAddr, DkmReadMemoryFlags.None, debugeeConnectivitysData);

                    fixed (byte* debugeeOffsetsDataPtr = debugeeOffsetsData)
                    {
                        fixed (byte* debugeeConnectivitysDataPtr = debugeeConnectivitysData)
                        {
                            if (debugeeElemSizeInBytes == sizeof(int))
                            {
                                vtkPolyDataVS.CopyArrayCast((int*)debugeeConnectivitysDataPtr, nCnnSize, (long*)connectivity);
                                vtkPolyDataVS.CopyArrayCast((int*)debugeeOffsetsDataPtr, nCells + 1, (long*)offsets);                                
                            }
                            else if (debugeeElemSizeInBytes == sizeof(long))
                            {
                                vtkPolyDataVS.CopyArrayCast((long*)debugeeConnectivitysDataPtr, nCnnSize, (int*)connectivity);
                                vtkPolyDataVS.CopyArrayCast((long*)debugeeOffsetsDataPtr, nCells + 1, (int*)offsets);
                            }
                        }
                    }
                }
            }
        }

        private void ExtractPoints(VsDbgExpression expr, vtkPolyDataVS ret)
        {
            if (expr.Address.GetValueOrDefault() == 0)
                return; //no points available

            //we will need to call several methods to determine size of the data
            int nPoints = expr.CallMethod<int>("GetNumberOfPoints()");
            int nDataType = expr.CallMethod<int>("GetDataType()");
            ulong address = expr.CallMethodReturningPointer("GetVoidPointer(0)");

            unsafe
            {
                int sizeInBytes;
                var dstPtr = ret.AllocatePoints(nPoints, nDataType, &sizeInBytes);

                DkmProcess process = expr.InspectionContext.Thread.Process;
                int read = process.ReadMemory(address, DkmReadMemoryFlags.None, dstPtr, sizeInBytes);

                Debug.Assert(read == sizeInBytes);
            }
        }     
    }
}
