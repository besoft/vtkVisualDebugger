#pragma once

#include <msclr/marshal.h>
#include "vtkPolyDataVS.h"

#include "../../vtkVisDbgCore/vtkVisualDebugger.h"

namespace vtkVisDbg
{
	public ref class vtkVisualDebuggerVS : IDisposable
	{
	private:
		static vtkVisualDebuggerVS^ g_VsDebugger;
		vtkVisualDebugger* m_VD;

	private:
		vtkVisualDebuggerVS()
		{
			m_VD = vtkVisualDebugger::New();
			m_VD->SetFullScreen(0);
		}

	public:
		~vtkVisualDebuggerVS()
		{
			this->!vtkVisualDebuggerVS();
		}

		!vtkVisualDebuggerVS()
		{
			if (m_VD != nullptr)
			{
				m_VD->Delete();
				m_VD = nullptr;
			}
		}

	private:
		static vtkVisualDebugger* GetVD()
		{
			if (g_VsDebugger == nullptr)
				g_VsDebugger = gcnew vtkVisualDebuggerVS();

			return g_VsDebugger->m_VD;
		}

	public:
		static int AddPolyData(vtkPolyDataVS^ poly)
		{
			msclr::interop::marshal_context oMarshalContext;

			if (poly->Data->GetNumberOfCells() == 0)
			{
				if (poly->Data->GetNumberOfPoints() != 0)
				{
					//we assume that the points belong to some surface that is 
					//more or less uniformly sampled, the density is thus 
					//the square root of the number of points
					double dns = sqrt(poly->Data->GetNumberOfPoints());

					GetVD()->AddOrUpdatePoints(oMarshalContext.marshal_as<const char*>(poly->Name), poly->Data->GetPoints(),
						poly->Data->GetLength() / (2*dns),
						1.0, 0.5, 0.0 //r,g, b
					);
				}
			}

			if (poly->Data->GetNumberOfPolys() != 0) {
				GetVD()->AddOrUpdateSurface(oMarshalContext.marshal_as<const char*>(poly->Name), poly->Data, 0.5, 0.5, 0.5);
			}

			if (poly->Data->GetNumberOfLines() != 0) 
			{
				//we assume the cells form long poly-lines				
				GetVD()->AddOrUpdateLines(oMarshalContext.marshal_as<const char*>(poly->Name), poly->Data, 
					poly->Data->GetLength() / 100,
					0.5, 0.0, 1.0 //r, g, b
				);
			}

			return 0;
		}

		static void ShowDialog()
		{
			auto vd = GetVD();
			vd->DebugStep();

			if (vd->IsStopped())
			{
				//the debugging is over, so release VD
				delete g_VsDebugger;
				g_VsDebugger = nullptr;
			}
		}
	};
}

