#pragma once

#include "vtkPolyData.h"
#include "vtkCellArray.h"
#include "vtkIdTypeArray.h"

using namespace System;

namespace vtkVisDbg
{	
	public enum class vtkPolyDataVSCellType
	{
		Verts,
		Lines,
		Polys,
		Strips
	};

	public ref class vtkPolyDataVS : IDisposable
	{
	public:
		vtkPolyDataVS(String^ name)
		{
			m_Name = name;
			m_Data = vtkPolyData::New();
		}


		~vtkPolyDataVS()
		{
			this->!vtkPolyDataVS();
		}

		!vtkPolyDataVS()
		{
			if (m_Data != nullptr)
			{
				m_Data->Delete();
				m_Data = nullptr;
			}
		}

	public:
		/* Allocates the points and return the pointer to internal array of floats
		so it could be filled by the caller using direct mem to mem copying */
		void* AllocatePoints(int count, int& size)
		{
			return AllocatePoints(count, VTK_FLOAT, size);
		}
		
		/* Allocates the points and return the pointer to internal array of 
		floats or doubles, depending on dataType, so it could be filled 
		by the caller using direct mem to mem copying */
		void* AllocatePoints(int count, int dataType, int& sizeInBytes)
		{
			vtkPoints* points = vtkPoints::New(dataType);
			m_Data->SetPoints(points);
			points->Delete();

			points->SetNumberOfPoints(count);			
			sizeInBytes = points->GetData()->GetDataSize() *
				points->GetData()->GetDataTypeSize();

			return points->GetVoidPointer(0);
		}

		/* Allocates the cells and returns two pointers to internal arrays
		* of Int32 or Int64, one for the connectivity (capable of storing
		* connectivitySize elements), the other for offsets (i.e., indices,
		* to the connectivity array where the cell starts; capable of
		* storing numCells cells).
		*/
		void AllocateCells(int numCells, int connectivitySize, 
			vtkPolyDataVSCellType kind, 
			int& elemSizeInBytes, void*& connectivity, void*& offsets)
		{
			vtkCellArray* ca = vtkCellArray::New();			
			switch (kind)
			{
			case vtkVisDbg::vtkPolyDataVSCellType::Verts:
				m_Data->SetVerts(ca);
				break;
			case vtkVisDbg::vtkPolyDataVSCellType::Lines:
				m_Data->SetLines(ca);
				break;
			case vtkVisDbg::vtkPolyDataVSCellType::Polys:
				m_Data->SetPolys(ca);
				break;
			case vtkVisDbg::vtkPolyDataVSCellType::Strips:
				m_Data->SetStrips(ca);
				break;
			default:
				break;
			}

			ca->AllocateExact(numCells, connectivitySize);
			
			vtkDataArray* connDA = ca->GetConnectivityArray();
			vtkDataArray* offsDA = ca->GetOffsetsArray();

			elemSizeInBytes = connDA->GetElementComponentSize();
			connectivity = connDA->GetVoidPointer(0);
			offsets = offsDA->GetVoidPointer(0);			

			ca->Delete();			
		}

		/** Helper methods*/
		static void LegacyCells_To_V2(int* legacyData, int size, int* connectivity, int* offset)
		{
			LegacyCells_To_V2_T<int, int>(legacyData, size, connectivity, offset);
		}

		static void LegacyCells_To_V2(int* legacyData, int size, long long* connectivity, long long* offset)
		{
			LegacyCells_To_V2_T<int, long long>(legacyData, size, connectivity, offset);
		}

		static void LegacyCells_To_V2(long long* legacyData, int size, int* connectivity, int* offset)
		{
			LegacyCells_To_V2_T<long long, int>(legacyData, size, connectivity, offset);
		}

		static void LegacyCells_To_V2(long long* legacyData, int size, long long* connectivity, long long* offset)
		{
			LegacyCells_To_V2_T<long long, long long>(legacyData, size, connectivity, offset);
		}

		static void CopyArrayCast(int* source, int size, long long* dest)
		{
			CopyArrayCast_T(source, size, dest);
		}

		static void CopyArrayCast(long long* source, int size, int* dest)
		{
			CopyArrayCast_T(source, size, dest);
		}

	private:
		template <typename T1, typename T2>
		static void LegacyCells_To_V2_T(T1* legacyData, int size, T2* connectivity, T2* offset)
		{
			*offset = 0;
			while (size != 0)
			{
				offset++; size--;
				*offset = *legacyData;

				for (int i = 0; i < *offset; i++)
				{
					*connectivity = *legacyData;
					connectivity++;
					legacyData++;
					size--;
				}
			}
		}

		template <typename T1, typename T2>
		static void CopyArrayCast_T(T1* source, int size, T2* dest)
		{
			for (int i = 0; i < size; i++)
			{
				dest[i] = (T2)source[i];
			}
		}


	public:
		property String^ Name {
			String^ get() {
				return m_Name;
			}
		}

		property vtkPolyData* Data {
			vtkPolyData* get() {
				return m_Data;
			}
		}
	private:
		vtkPolyData* m_Data;
		String^ m_Name;
	};
}

