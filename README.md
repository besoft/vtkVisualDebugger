# Introduction
Developing a complicated method to process scientific data usually requires a good understanding of the behaviour of the algorithms used in this method.  As the data are often big and in 3D, this might be difficult. Take a smoothing of surface meshes as an example. It is easy to spot that the developed method (e.g., Lei He and Scott Schaefer 2013: Mesh denoising via L0 minimization) sometimes provides the smooth meshes with a few sharp peaks. Is this because of the limitation of the method or a bug in the implementation? Probably a bug, unless the inputs for which the artefacts are present differ from those for which the results are as expected. Using traditional debugging tools may need hours to find the source of the problem. What we need is to inspect the changing mesh visually.

VTK Visual Debugger aims at providing the developers of the applications based on VTK (http://vtk.org) toolkit with the ability to debug the VTK objects visually. 

## Building
### Requirements
* CMake 3.9 or later (https://cmake.org)
* VTK (https://vtk.org)

### Standard Approach
Use CMake (cmake-gui) to configure VTK Visual Debugger and generate a project for it. Open the project and build it. 

### Visual Studio 2017
**Note: this assumes you are familiar with CMake command line**

If you are using Microsoft Visual Studio 2017, you may open the source codes of the VTK Visual Debugger as a CMake Project (see https://docs.microsoft.com/en-us/cpp/ide/cmake-tools-for-visual-cpp and https://docs.microsoft.com/en-us/cpp/ide/non-msbuild-projects). As the VTK Visual Debugger depends on the VTK library and inherently may also depend on Intel TBB, you will probably need to update the default CMake configurations Visual Studio provided for you. An example of the updated configuration is here:

```json
  "environments": [
    {
      "VTK_INSTALL_ROOTDIR": "${workspaceRoot}\\..\\..\\Libraries\\VTK\\Bin",
      "VTK_VERSION": "vtk-9.0",

      "TBB_INSTALL_ROOTDIR": "${workspaceRoot}\\..\\..\\Libraries\\IntelTBB\\Build\\vs2017"
    }
  ],
  "configurations": [
    {
      "name": "x86-Debug",
      "generator": "Ninja",
      "configurationType": "Debug",
      "inheritEnvironments": [ "msvc_x86" ],
      "buildRoot": "${workspaceRoot}\\build\\${name}",
      "installRoot": "${workspaceRoot}\\bin\\${name}",
      "cmakeCommandArgs": "",
      "buildCommandArgs": "-v",
      "ctestCommandArgs": "",
      "environments": [
        {
          "VTK_INSTALL_DIR": "${env.VTK_INSTALL_ROOTDIR}\\x86-Debug",
          "TBB_INSTALL_DIR": "${env.TBB_INSTALL_ROOTDIR}\\Win32\\Debug-MT"
        }               
      ],
      "variables": [
        {
          "name": "VTK_DIR",
          "value": "${env.VTK_INSTALL_DIR}\\lib\\cmake\\${env.VTK_VERSION}"
        }
      ]
    },
```

Once the Visual Studio builds the VTK Visual Debugger binaries, you may also need to create launch.vs.json to specify the executables and the paths to VTK binaries using the undocumented configuration element:
```json
"env": "PATH=${env.VTK_INSTALL_DIR}\\bin;${env.TBB_INSTALL_DIR}"
```