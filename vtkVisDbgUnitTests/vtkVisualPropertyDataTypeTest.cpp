#include "pch.h"
#include "CppUnitTest.h"
#include "../vtkVisDbgCore/Model/vtkVisualPropertyDataType.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace vtkVisDbgUnitTest
{
	TEST_CLASS(vtkVisualPropertyDataTypeTest)
	{
	public:

		TEST_METHOD(vtkVisualPropertyDataTypeValue_Test)
		{
			vtkVisDbg::vtkVisualPropertyDataTypeValue<int> IntVal;
			Assert::IsFalse(IntVal.IsInitialized());
			Assert::IsFalse(IntVal.IsModified());			
			Assert::IsTrue(IntVal.GetDataType() == vtkVisDbg::vtkVisualPropertyDataType::Int32);

			vtkVisDbg::vtkVisualPropertyDataTypeVoidValue* a = IntVal.NewInstance();
			Assert::IsFalse(a->IsInitialized());
			Assert::IsFalse(a->IsModified());
			Assert::IsTrue(a->GetDataType() == vtkVisDbg::vtkVisualPropertyDataType::Int32);
			Assert::IsTrue(a->CompareTo(&IntVal) == 0);	//both are unset

			vtkVisDbg::vtkVisualPropertyDataTypeValue<int> IntVal2(5);
			Assert::IsTrue(IntVal2.IsInitialized());
			Assert::IsFalse(IntVal2.IsModified());
			Assert::IsTrue(a->CompareTo(&IntVal2) < 0);	//unset goes first
			Assert::IsTrue(IntVal2.CompareTo(a) > 0);	//unset goes first

			a->Copy(&IntVal2);
			Assert::IsTrue(a->IsInitialized());
			Assert::IsTrue(a->IsModified());
			Assert::IsTrue(a->CompareTo(&IntVal2) == 0);	//unset goes first
			delete a;

			a = IntVal2.Clone();
			Assert::IsTrue(a->IsInitialized());
			Assert::IsFalse(a->IsModified());
			Assert::IsTrue(a->CompareTo(&IntVal2) == 0);	//both should be 5
			delete a;

			vtkVisDbg::vtkVisualPropertyDataTypeValue<int> IntVal3(17);
			Assert::IsTrue(IntVal2.CompareTo(&IntVal3) < 0);	//5 < 17
			Assert::IsTrue(IntVal3.CompareTo(&IntVal2) > 0);	//5 < 17

			vtkVisDbg::vtkVisualPropertyDataTypeValue<double> DblVal(-5.2);
			vtkVisDbg::vtkVisualPropertyDataTypeValue<double> DblVal2(2.0);
			Assert::IsTrue(DblVal.CompareTo(&DblVal2) < 0);	//-5.2 < 2
			Assert::IsTrue(DblVal2.CompareTo(&DblVal) > 0);	//-5.2 < 2

			double d = DblVal.GetValue();
			Assert::AreEqual(d, -5.2);
		}

		TEST_METHOD(vtkVisualPropertyDataTypeVecValue_Test)
		{
			vtkVisDbg::vtkVisualPropertyDataTypeVecValue<int> IntVal;
			Assert::IsFalse(IntVal.IsInitialized());
			Assert::IsFalse(IntVal.IsModified());
			Assert::IsTrue(IntVal.GetDataType() == vtkVisDbg::vtkVisualPropertyDataType::Vec3Int32);

			vtkVisDbg::vtkVisualPropertyDataTypeVoidValue* a = IntVal.NewInstance();
			Assert::IsFalse(a->IsInitialized());
			Assert::IsFalse(a->IsModified());
			Assert::IsTrue(a->GetDataType() == vtkVisDbg::vtkVisualPropertyDataType::Vec3Int32);
			Assert::IsTrue(a->CompareTo(&IntVal) == 0);	//both are unset

			vtkVisDbg::vtkVisualPropertyDataTypeVecValue<int> IntVal2(0, 1, 2);
			Assert::IsTrue(IntVal2.IsInitialized());
			Assert::IsFalse(IntVal2.IsModified());
			Assert::IsTrue(a->CompareTo(&IntVal2) < 0);	//unset goes first
			Assert::IsTrue(IntVal2.CompareTo(a) > 0);	//unset goes first

			a->Copy(&IntVal2);
			Assert::IsTrue(a->IsInitialized());
			Assert::IsTrue(a->IsModified());
			Assert::IsTrue(a->CompareTo(&IntVal2) == 0);	//unset goes first
			delete a;

			a = IntVal2.Clone();
			Assert::IsTrue(a->IsInitialized());
			Assert::IsFalse(a->IsModified());
			Assert::IsTrue(a->CompareTo(&IntVal2) == 0);
			delete a;

			vtkVisDbg::vtkVisualPropertyDataTypeVecValue<int> IntVal3(0,2,4);
			Assert::IsTrue(IntVal2.CompareTo(&IntVal3) < 0);	//0,1,2 < 0,2,4
			Assert::IsTrue(IntVal3.CompareTo(&IntVal2) > 0);	//0,1,2 < 0,2,4

			IntVal3.SetValue({ 0, 0, 0 });
			const int* val = IntVal3.GetValue();
			Assert::IsTrue(val[0] + val[1] + val[2] == 0);
		}

		TEST_METHOD(vtkVisualPropertyDataTypeStringValue_Test)
		{
			vtkVisDbg::vtkVisualPropertyDataTypeStringValue StrVal;
			Assert::IsFalse(StrVal.IsInitialized());
			Assert::IsFalse(StrVal.IsModified());
			Assert::IsTrue(StrVal.GetDataType() == vtkVisDbg::vtkVisualPropertyDataType::String);

			vtkVisDbg::vtkVisualPropertyDataTypeVoidValue* a = StrVal.NewInstance();
			Assert::IsFalse(a->IsInitialized());
			Assert::IsFalse(a->IsModified());
			Assert::IsTrue(a->GetDataType() == vtkVisDbg::vtkVisualPropertyDataType::String);
			Assert::IsTrue(a->CompareTo(&StrVal) == 0);	//both are unset

			vtkVisDbg::vtkVisualPropertyDataTypeStringValue StrVal2("Hello");
			Assert::IsTrue(StrVal2.IsInitialized());
			Assert::IsFalse(StrVal2.IsModified());
			Assert::IsTrue(a->CompareTo(&StrVal2) < 0);	//unset goes first
			Assert::IsTrue(StrVal2.CompareTo(a) > 0);	//unset goes first

			a->Copy(&StrVal2);
			Assert::IsTrue(a->IsInitialized());
			Assert::IsTrue(a->IsModified());
			Assert::IsTrue(a->CompareTo(&StrVal2) == 0);	//unset goes first
			delete a;

			a = StrVal2.Clone();
			Assert::IsTrue(a->IsInitialized());
			Assert::IsFalse(a->IsModified());
			Assert::IsTrue(a->CompareTo(&StrVal2) == 0);
			delete a;

			vtkVisDbg::vtkVisualPropertyDataTypeStringValue StrVal3("World");
			Assert::IsTrue(StrVal2.CompareTo(&StrVal3) < 0);	//Hello < World
			Assert::IsTrue(StrVal3.CompareTo(&StrVal2) > 0);	//Hello < World

			Assert::AreEqual(StrVal3.GetValue(), "World");
			StrVal3.SetValue(StrVal2.GetValue());
			Assert::AreEqual(StrVal3.GetValue(), "Hello");
		}
	};
}
