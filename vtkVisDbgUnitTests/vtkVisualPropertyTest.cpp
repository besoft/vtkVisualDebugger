#include "pch.h"
#include "CppUnitTest.h"
#include "vtkObjectFactory.h"
#include "../vtkVisDbgCore/Model/vtkVisualProperty.h"
#include "../vtkVisDbgCore/Model/vtkVisualObject.h"
#include "../vtkVisDbgCore/Internals/vtkVisualPropertyRegistrator.h"
#include "vtkNew.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace vtkVisDbg;

namespace vtkVisDbgUnitTest
{
	//test class
	class vtkVisualObjectDerived : public vtkVisDbg::vtkVisualObject
	{
	public:
		vtkTypeMacro(vtkVisualObjectDerived, vtkVisDbg::vtkVisualObject);		
		static vtkVisualObjectDerived* New();

		static vtkVisDbg::vtkVisualProperty<double> Radius;
		static vtkVisDbg::vtkVisualPropertyVector<double> Point;
		static vtkVisDbg::vtkVisualPropertyString Name;
	};

	vtkStandardNewMacro(vtkVisualObjectDerived);

#define DEFAULT_RADIUS 0.5

	CONSTRUCT_VISUALPROPERTY(vtkVisualObjectDerived, Radius, DEFAULT_RADIUS, 10, false, "Radius must be <= 10.");
	CONSTRUCT_VISUALPROPERTY(vtkVisualObjectDerived, Point, 0, 0, 0, "Point origin.");
	CONSTRUCT_VISUALPROPERTY(vtkVisualObjectDerived, Name, "--- UNKNOWN ---", "Name of the object.");
	
	BEGIN_VISUALPROPERTY_REGISTRATION(vtkVisualObjectDerived)
		REGISTER_VISUALPROPERTY(vtkVisualObjectDerived, Name);
		REGISTER_VISUALPROPERTY(vtkVisualObjectDerived, Point);
		REGISTER_VISUALPROPERTY(vtkVisualObjectDerived, Radius);
	END_VISUALPROPERTY_REGISTRATION(vtkVisualObjectDerived);

	



	TEST_CLASS(vtkVisualPropertyTest)
	{
	public:

		TEST_METHOD(vtkVisualProperty_Test)
		{
			vtkVisDbg::vtkVisualProperty<bool> Visible("Visible", true);
			vtkVisDbg::vtkVisualProperty<int> Range("MaxRange", 10, 10, false, "MaxRange must be <= 10.");
			vtkVisDbg::vtkVisualPropertyVector<int> Color("Color", { 0,0,0 }, { 0,0,0 }, {255,255,255}, "Color in 8-bit RGB (0-255, 0-255, 0-255)");
			vtkVisDbg::vtkVisualPropertyVector<double> Origin("Origin", { 0,0,0 }, { 0,0,0 }, true, "Origin cannot be negative");
			vtkVisDbg::vtkVisualPropertyString Name("Name", "");

			auto propVal = Visible.CreatePropertyValue();
			Visible.SetValue(propVal, false);

			bool newVal = Visible.GetValue(propVal);
			Assert::IsTrue(newVal == false);
			
			delete propVal;

			propVal = Origin.CreatePropertyValue();
			Origin.SetValue(propVal, { 1, 1, 1 });
			const double* obtained = Origin.GetValue(propVal);
			Assert::IsTrue(obtained[0] == obtained[1] && obtained[1] == obtained[2] && obtained[2] == 1);

			delete propVal;
		}

		TEST_METHOD(vtkVisualObject_GetSetTest)
		{
			/*
			* root --> o1 -\
			*				---> o2
			*	  \--> o3 -/	
			*/
			
			vtkNew<vtkVisualObjectDerived> root;
			vtkNew<vtkVisualObjectDerived> o1;
			vtkNew<vtkVisualObjectDerived> o2;
			vtkNew<vtkVisualObjectDerived> o3;
			root->AddChild(o1);
			root->AddChild(o3);
			o1->AddChild(o2);
			o3->AddChild(o2);
			
			//check the structure
			Assert::IsTrue(root->GetChildrenCount() == 2);
			Assert::IsTrue(o2->GetParentsCount() == 2);
			Assert::IsTrue(root->GetChild(1) == o3);
			
			//set o1
			double RADIUS = 4.25;
			o1->SetValue(vtkVisualObjectDerived::Radius, RADIUS);
			Assert::IsTrue(o1->GetValue(vtkVisualObjectDerived::Radius) == RADIUS);
			
			Assert::IsTrue(o3->GetValue(vtkVisualObjectDerived::Radius) == DEFAULT_RADIUS);	//default
			Assert::IsTrue(o2->GetValue(vtkVisualObjectDerived::Radius) == RADIUS);	//inherited

			/*
			vtkVisDbg::vtkVisualObject* o = vtkVisDbg::vtkVisualObject::New();
			int r = o->GetValue(Range);
			obtained = o->GetValue(Origin);
			const char* n = o->GetValue(Name);

			o->Delete();
			*/
		}
	};
}
