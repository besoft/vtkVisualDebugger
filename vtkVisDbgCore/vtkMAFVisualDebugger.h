/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: vtkMAFVisualDebugger.cpp,v $
Language:  C++
Date:      $Date: 2012-04-30 14:52:43 $
Version:   $Revision: 1.0 $
Authors:   Josef Kohout,  Jana H�jkov�
==========================================================================
Copyright (c) 2012 University of University of West Bohemia
See the COPYINGS file for license details 
=========================================================================
Simple helper class for Visual debugging of methods.
=========================================================================*/

#ifndef __vtkMAFVisualDebugger_h
#define __vtkMAFVisualDebugger_h

#include "vtkVisDbgDefs.h"
#include "vtkObject.h"

#include <functional>

#ifndef VTKVD_USE_PIMPL
#include "Internals/vtkMAFVisualDebuggerInternal.h"
#endif

//forward declarations
class vtkPolyData;
class vtkPoints;
class vtkRenderer;
class vtkProp;
class vtkRenderWindow;
class vtkRenderWindowInteractor;
class vtkTransform;
class vtkMatrix4x4;

#ifdef VTKVD_USE_PIMPL
	class vtkMAFVisualDebuggerInternal;
#endif

	class vtkVisualObject_Old;

	/** Use this class to display an interactive rendering window that allows you
	to show the current data using various visualization techniques without
	necessity to implement complex visual pipes (these pipes are created by this class)*/
	class vtkVisDbg_EXPORTS vtkMAFVisualDebugger : public vtkObject
	{
	public:
		vtkTypeMacro(vtkMAFVisualDebugger, vtkObject);

	protected:
		enum FrameNavigation {
			StopDebugging = -1,	//debugger is to be disabled, further calls of DebugStep are ignored		
			NextStep = 0,				//each step is displayed		
			///positive values 1, ... denote how may calls (of the same part) should be ignored
		};

		FrameNavigation m_NavigationState;		///<Current  navigation state
		int m_NumberOfIgnoredSteps;						///<number of steps to be ignored
		int m_CurrentFrameId;									///<Id of the current frame

		int WindowLeft;											///<position of window
		int WindowTop;											///<	position of window
		int WindowWidth;										///<	size of window
		int WindowHeight;										///<	size of window
		int FullScreen;											///<	non-zero, if the window is fullscreen	

		double BackgroundColor[3];				///<Background color
		bool ViewPersistency;					///<should we keep the view position persistent
		char* ViewPersistencyFilename;			///<the filename containing the viewport information
		bool ScreenShotOnESC;					///<should we take screenshots upon ESC
		char* ScreenShotOnESCFilename;			///<the filename where to store the screenshot
		bool AutoPressESC;						///<true, if ESC should be automatically emulated

		vtkRenderer* m_Renderer;							///<renderer
		vtkRenderWindowInteractor* m_RendererInteractor;	///<rendering window manipulator		
		std::function<void(void)> m_TimerCallback;			///<user specified timer callback

		bool m_HelpDisplayed;								///<true, if the help is displayed
		

		double m_TotalResizeCoef;				///<coefficient for resizing

		///<variables from GUI to skip some frames
		int m_nSkipIndex;					///<next index to m_szSkip where character should be placed -1, if m_NumberOfIgnoredSteps
		char m_szSkip[16];				///<entered number of frames to be skipped

	protected:
		/** Constructor */
		vtkMAFVisualDebugger();

		/** Destructor */
		virtual ~vtkMAFVisualDebugger();

	public:
		/** Creates an instance of the object */
		static vtkMAFVisualDebugger *New();

		/** Gets the current position of the renderer window*/
		vtkGetMacro(WindowLeft, int);

		/** Gets the current position of the renderer window*/
		vtkGetMacro(WindowTop, int);

		/** Gets the current size of the renderer window*/
		vtkGetMacro(WindowWidth, int);

		/** Gets the current size of the renderer window*/
		vtkGetMacro(WindowHeight, int);

		/** Sets the new position of the renderer window*/
		vtkSetMacro(WindowLeft, int);

		/** Sets the new position of the renderer window*/
		vtkSetMacro(WindowTop, int);

		/** Sets the new size of the renderer window*/
		vtkSetMacro(WindowWidth, int);

		/** Sets the new size of the renderer window*/
		vtkSetMacro(WindowHeight, int);

		/** Sets the whether the renderer window should be displayed in fullscreen*/
		vtkSetMacro(FullScreen, int);

		/** Gets the current background color*/
		vtkGetVector3Macro(BackgroundColor, double);

		/** Sets the new background color*/
		vtkSetVector3Macro(BackgroundColor, double);

		/** Gets/sets whether the camera, windows, etc. should be persistent. 
		When enabled, the configuration is loaded/stored from/into the file 
		with the pathname ViewPersistencyFilename.
		Default is OFF.*/
		vtkGetMacro(ViewPersistency, bool);
		vtkSetMacro(ViewPersistency, bool);
		vtkBooleanMacro(ViewPersistency, bool);

		/** Gets/sets the filename where is stored the configuration.
		Default is nullptr. */
		vtkGetStringMacro(ViewPersistencyFilename);
		vtkSetStringMacro(ViewPersistencyFilename);

		/** Gets/sets whether a screenshot should be taken when the user press ESC.
		When enabled, the screenshot is taken into the file ScreenShotOnESCFilename.		
		Default is OFF.*/
		vtkGetMacro(ScreenShotOnESC, bool);
		vtkSetMacro(ScreenShotOnESC, bool);
		vtkBooleanMacro(ScreenShotOnESC, bool);

		/** Gets/sets the filename where the screenshot should be stored when ESC is pressed.
		Default is nullptr. */
		vtkGetStringMacro(ScreenShotOnESCFilename);
		vtkSetStringMacro(ScreenShotOnESCFilename);

		/** Gets/sets whether the system should automatically emulate pressing ESC after showing the window.
		NOTE: this is the hack allowing to getting screenshots while avoiding the necessity of manual pressing ESC
		Default is OFF.*/
		vtkGetMacro(AutoPressESC, bool);
		vtkSetMacro(AutoPressESC, bool);
		vtkBooleanMacro(AutoPressESC, bool);

		/** Returns true, if the debugging has been stopped and calling DebugStep will be ignored. */
		inline bool IsStopped() {
			return m_NavigationState == vtkMAFVisualDebugger::StopDebugging;
		}

		/** Returns true, if the next calling of DebugStep will display the data. */
		inline bool IsStepping() {
			return m_NavigationState == vtkMAFVisualDebugger::NextStep;
		}

		/** Opens the rendering window displaying everything and interacting with the user.
		The method ends when the user expresses his/her wishes to continue with the method being debugged.	*/
		virtual void DebugStep(std::function<void(void)> onTimerCallback = nullptr) {
			DebugStep(false, onTimerCallback);
		}

		/** Opens the rendering window displaying everything and interacting with the user.
		The method ends when the user expresses his/her wishes to continue with the method being debugged.
		If keepRenderingWindow is true, the rendering window is not closed but remains so that is could
		be reused without flickering in the next DebugStep. */
		virtual void DebugStep(bool keepRenderingWindow, std::function<void(void)> onTimerCallback = nullptr);

		/** Adds some label with the given Id into the scene.
		If an object with the same Id already exists, it is updated.
		The label is displayed using default font family and the given font size
		in (r, g, b) colour at position x and y. Position is relative to the specified
		horizontal and vertical alignment (parameters horiz_align and vert_align).
		Valid values of alignment are -1, 0, 1 denoting LEFT, CENTER, RIGHT
		or TOP, CENTER, BOTTOM alignment. */
		virtual void AddOrUpdateLabel(const char* Id,
			const char* label, int fontsize,
			double r, double g, double b,
			double x = 0.01, double y = 0.01,
			int horiz_align = -1, int vert_align = 1);

		/** Adds scalar field with the given Id into the scene.
		If an object with the same Id already exists, it is updated.
		Legend is displayed, if displaybar is true, in which case legend is shown
		at x, y position relative to the window size and values of horiz_align and
		vert_align - see also AddOrUpdateLabel method.*/
		virtual void AddOrUpdateScalarField(const char* Id,
			vtkPolyData* poly, bool displaybar = false,
			double x = 0.1, double y = 0.01,
			int horiz_align = -1, int vert_align = -1);

		/** Adds surface mesh with the given Id into the scene.
		If an object with the same Id already exists, it is updated.
		(R,G,B) is colour in which the mesh is to be rendered.
		Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.*/
		virtual void AddOrUpdateSurface(const char* Id,
			vtkPolyData* poly,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false);

		/** Adds points with the given Id into the scene.
		If an object with the same Id already exists, it is updated.
		Radius = radius of spheres to be rendered for the given points,
		(R,G,B) is colour in which spheres are to be rendered.
		Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
		If bCubeglyph is true, small cubes are rendered instead of spheres. */
		virtual void AddOrUpdatePoints(const char* Id,
			vtkPolyData* points, double radius,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false, bool bcubeglyph = false);

		/** Adds points with the given Id into the scene.
		If an object with the same Id already exists, it is updated.
		Radius = radius of spheres to be rendered for the given points,
		(R,G,B) is colour in which spheres are to be rendered.
		Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
		If bCubeglyph is true, small cubes are rendered instead of spheres.*/
		virtual void AddOrUpdatePoints(const char* Id,
			vtkPoints* points, double radius,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false, bool bcubeglyph = false);

		/** Adds points with the given Id into the scene.
		If an object with the same Id already exists, it is updated.
		Points = array of points in format (x1,y1,z1, x2, y2, z2, ...)
		Count = number of points in points array
		Radius = radius of spheres to be rendered for the given points,
		(R,G,B) is colour in which spheres are to be rendered.
		Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
		If bCubeglyph is true, small cubes are rendered instead of spheres.*/
		virtual void AddOrUpdatePoints(const char* Id,
			const double* points, int count, double radius,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false, bool bcubeglyph = false);

		/** Equivalent to AddOrUpdatePoints method with a small difference: this method visualize
		only the points having their indices in validIndices array containing  validIndicesCount entries.
		Note: validIndices can be easily retrieved from vtkIdList. */
		virtual void AddOrUpdateSelectedPoints(const char* Id,
			vtkPolyData* points, const vtkIdType* validIndices,
			int validIndicesCount, double radius,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false, bool bcubeglyph = false);

		/** Equivalent to AddOrUpdatePoints method with a small difference: this method visualize
		only the points having their indices in validIndices array containing  validIndicesCount entries.
		Note: validIndices can be easily retrieved from vtkIdList. */
		virtual void AddOrUpdateSelectedPoints(const char* Id,
			vtkPoints* points, const vtkIdType* validIndices,
			int validIndicesCount, double radius,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false, bool bcubeglyph = false);

		/** Equivalent to AddOrUpdatePoints method with a small difference: this method visualize
		only the points having their indices in validIndices array containing  validIndicesCount entries.
		Note: validIndices can be easily retrieved from vtkIdList. */
		virtual void AddOrUpdateSelectedPoints(const char* Id,
			const double* points, int count, const vtkIdType* validIndices,
			int validIndicesCount, double radius,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false, bool bcubeglyph = false);

		/** Adds lines with the given Id into the scene.
		If an object with the same Id already exists, it is updated.
		Radius = radius of tubes to be rendered for the given poly-lines,
		(R,G,B) is colour in which tubes are to be rendered.
		Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.*/
		virtual void AddOrUpdateLines(const char* Id,
			vtkPolyData* polylines, double radius,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false);

		/** Adds lines with the given Id into the scene.
		If an object with the same Id already exists, it is updated.
		PolyLine contains the connectivity of the poly-line, polyLineCount is number of entries.
		Radius = radius of tubes to be rendered for the given poly-lines,
		(R,G,B) is colour in which tubes are to be rendered.
		Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.*/
		virtual void AddOrUpdateLines(const char* Id,
			vtkPoints* points, const vtkIdType* polyLine,
			int polyLineCount, double radius,
			double r, double g, double b,
			double opacity = 1.0, bool wireframe = false);

		/** Adds a single axis of the given length. */
		virtual void AddOrUpdateAxis(const char* Id, const double origin[3], const double direction[3],
			double tipRadius, double axisLength, double r, double g, double b);

		/** Adds a local frame system.
		If originTr is specified, the system is translated to the specified position.*/
		virtual void AddOrUpdateLocalFrame(const char* Id, vtkTransform* tr,
			double tipRadius, double axisLength, const double* originTr = NULL);

		/** Adds a local frame system.
		If originTr is specified, the system is translated to the specified position.*/
		virtual void AddOrUpdateLocalFrame(const char* Id, vtkMatrix4x4* m,
			double tipRadius, double axisLength, const double* originTr = NULL);

		/** Adds external actor with the given Id into the scene.
		If an object with the same Id already exists, it is updated. */
		virtual void AddOrUpdateExternalObject(const char* Id, vtkProp* actor);


		/** Removes label with the given Id from the scene. */
		inline void RemoveLabel(const char* Id) {
			RemoveVisualObject(Id);
		}


		/** Removes the scalar field with the given Id from the scene. */
		inline void RemoveScalarField(const char* Id) {
			RemoveVisualObject(Id);
		}

		/** Removes surface with the given Id from the scene. */
		inline void RemoveSurface(const char* Id) {
			RemoveVisualObject(Id);
		}

		/** Removes points with the given Id from the scene. */
		inline void RemovePoints(const char* Id) {
			RemoveVisualObject(Id);
		}

		/** Removes points with the given Id from the scene. */
		inline void RemoveSelectedPoints(const char* Id) {
			RemoveVisualObject(Id);
		}


		/** Removes lines with the given Id from the scene. */
		inline void RemoveLines(const char* Id) {
			RemoveVisualObject(Id);
		}

		/** Removes the external object with the given Id from the scene. */
		inline void RemoveExternalObject(const char* Id) {
			RemoveVisualObject(Id);
		}

		/** Removes all objects from the scene. */
		virtual void RemoveAll();


		/** Enables/Disables pickable ability of the visual object with the given Id. 
		N.B. Default is false to speed-up the rendering. */
		void EnablePickable(const char* Id, bool enable = true);


	protected:
		/** Adds the new visual object into the scene or updates it, if the object already exists in the scene.
		N.B. obj must be allocated on the heap and will be removed automatically when
		corresponding RemoveVisualObject or	RemoveAll is called. */
		virtual void AddOrUpdateVisualObject(const char* Id, vtkVisualObject_Old* obj);

		/** Removes the visual object from the scene. */
		virtual void RemoveVisualObject(const char* Id);
		

	private:
		/** Creates the render window. */
		vtkRenderWindow * CreateRenderWindow();

		/** Makes the screen shot of the render window and saves it to the specified file. */
		void ScreenShot(const char* fileName, vtkRenderWindow* renWin);

		/** Callback for visualization */
		static void KeypressCallback(vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* clientData, void* vtkNotUsed(callData));

		/** Callback for visualization */
		static void TimerCallback(vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* clientData, void* vtkNotUsed(callData));

		/** Callback for visualization */
		static void PickCallback(vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* clientData, void* vtkNotUsed(callData));

		/** Removes the visual object from the scene (without removing its reference). */
		void RemoveVisualObject(vtkVisualObject_Old* obj);

	private:
		vtkMAFVisualDebugger(const vtkMAFVisualDebugger&);  // Not implemented.
		void operator = (const vtkMAFVisualDebugger&);  // Not implemented.  

	protected:
		vtkMAFVisualDebuggerInternal * m_Internal;   //!< PIMPL mechanism to avoid DLLs problems due to STD		
#ifndef VTKVD_USE_PIMPL
	private:
		vtkMAFVisualDebuggerInternal _InternalObj;  //!< PIMPL aggregated to support inlining of methods for statically linked libraries
#endif

		friend class vtkMAFVisualDebuggerInternal;
	};

#endif //__vtkMAFVisualDebugger_h