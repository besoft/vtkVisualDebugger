#pragma once

#include "../vtkVisDbgDefs.h"
#include "vtkVisualPropertyDataType.h"

namespace vtkVisDbg
{	
	class vtkVisualPropertyValue;
		
	/**
	This is the base class for the visual properties.	
	*/
	class vtkVisDbg_EXPORTS vtkVisualPropertyKey
	{	
	protected:
		//constructor with no limitation of range
		//defaultValue MAY NOT BE NULL and will be released in dtor
		vtkVisualPropertyKey(const char* name,
			vtkVisualPropertyDataTypeVoidValue* defaultValue,
			const char* desc = nullptr)
			: vtkVisualPropertyKey(name, defaultValue, nullptr, nullptr, desc)
		{

		}

		//ctor with range limitation
		//defaultValue MAY NOT BE NULL and will be released in dtor
		//minValue and/or maxValue may be NULL and will be released in dtor
		vtkVisualPropertyKey(const char* name,
			vtkVisualPropertyDataTypeVoidValue* defaultValue,
			vtkVisualPropertyDataTypeVoidValue* minValue,
			vtkVisualPropertyDataTypeVoidValue* maxValue,
			const char* desc = nullptr);

	public:
		virtual ~vtkVisualPropertyKey();

	public:
		/** Get internally generated value that uniquely identifies the visual property. */
		int GetGlobalIndex() const {
			return m_GlobalIndex;
		}

		/** Get the name of the visual property. */
		const char* GetName() const {
			return m_Name;
		}

		/** Get the description of the visual property. */
		const char* GetDescription() const {
			return m_Description;
		}

		/** Get the data type of the visual property. */
		vtkVisualPropertyDataType GetDataType() const {
			return m_DefaultValue->GetDataType();
		}		

		/** Gets the default value */
		const vtkVisualPropertyDataTypeVoidValue* GetDefaultValue() const {
			return m_DefaultValue;
		}

		/** Sets new default value - MAY NOT BE NULL */
		void SetDefaultValue(vtkVisualPropertyDataTypeVoidValue* newDefaultValue);

		/** Gets the min value */
		const vtkVisualPropertyDataTypeVoidValue* GetMinValue() const {
			return m_MinValue;
		}

		/** Sets new min value */
		void SetMinValue(vtkVisualPropertyDataTypeVoidValue* newMinValue);

		/** Gets the max value */
		const vtkVisualPropertyDataTypeVoidValue* GetMaxValue() const {
			return m_MaxValue;
		}

		/** Sets new max value */
		void SetMaxValue(vtkVisualPropertyDataTypeVoidValue* newMaxValue);

		/** Creates a new instance of UnSet property value for this key. */
		vtkVisualPropertyValue* CreatePropertyValue() const;

	protected:
		int m_GlobalIndex;			//!< Internally generated value that uniquely identifies the visual property
		const char* m_Name;			//!< The name of the visual property
		const char* m_Description;	//!< The description of the visual property that can be used in comments
		
		vtkVisualPropertyDataTypeVoidValue* m_DefaultValue;	//!<default value of this property - may NOT be NULL
		vtkVisualPropertyDataTypeVoidValue* m_MinValue;		//!<min allowed value of this property - may be NULL
		vtkVisualPropertyDataTypeVoidValue* m_MaxValue;		//!<min allowed value of this property - may be NULL
	};
	
	class vtkVisDbg_EXPORTS vtkVisualPropertyValue final
	{
	private:
		struct Flags
		{
			int

				value_default : 1,		//value is unset or equals to default				
				value_valid : 1,		//value was validated and is valid [bit ignored if value is not set]
				value_coerced : 1,		//value was validated but has to be coerced to become valid [bit ignored if value is not set]				
				value_inheritable : 1;	//value is inheritable (otherwise it is private)
		};

	private:
		//vtkVisualPropertyValue can be created by vtkVisualPropertyKey only.
		vtkVisualPropertyValue(const vtkVisualPropertyKey* key) : m_Key(key),
			m_Flags({ 0,0,0,0, }) //all bits set to zero
		{
			m_Value = m_Key->GetDefaultValue()->NewInstance();	//create an unset instance
		}

	public:
		~vtkVisualPropertyValue() {
			delete m_Value;
		}

	public:

		/** TODO: revise the flags and their meaning. */

		/** Returns true, if the value is unset. */
		bool IsUnset() {
			return !m_Value->IsInitialized();
		};

		/** Returns true, if the value is the same as the default one. */
		bool IsDefault() {
			return IsUnset() || m_Flags.value_default != 0;
		};

		/** Returns true, if the value is valid (i.e., within the specified range, etc.). */
		bool HasBeenValidated() {
			return !m_Value->IsModified();
		}

		/** Returns true, if the value is valid (i.e., within the specified range, etc.). */
		bool IsValid() {
			return IsUnset() || m_Flags.value_valid != 0;
		}

		/** Returns true, if the value was coerced during the validation to become valid.
		For example, if the range is set to <0,1> and the value is 2, validation fails
		unless coercion is allowed in which case the value will be changed to 1 */
		bool IsCoerced() {
			return m_Flags.value_coerced != 0;
		}

		/** Returns true, if the value can be inherited by child visual object. */
		bool IsInheritable() {
			return m_Flags.value_inheritable != 0;
		}

		/** Returns true, if the value can be inherited by child visual object. */
		void SetInheritable(bool inheritable) {
			m_Flags.value_inheritable = inheritable;
		}

#if 0
		/** Performs validation of the currently set value.
		If the value is not valid, returns false and, if required, also errorMessage
		(to be deleted by the caller); otherwise returns true. */
		virtual bool Validate(bool allowCoerce = true, char** errorMessage = nullptr) = 0;
#endif	

		/** Returns the reference to the key of this property value. */
		const vtkVisualPropertyKey& GetKey() const {
			return *m_Key;
		}
		
		/** Returns the reference to the internal value container. */
		vtkVisualPropertyDataTypeVoidValue& GetValue() const {
			return *m_Value;
		}


	protected:
		const vtkVisualPropertyKey* m_Key;
		vtkVisualPropertyDataTypeVoidValue* m_Value;

		Flags m_Flags;					//!< Flags describing the current (executive) value		
		
		friend class vtkVisualPropertyKey;
	};



	/**
	This is type specific visual property key	
	*/
	template<typename T, size_t C = 1>
	class vtkVisualProperty : public vtkVisualPropertyKey
	{	
	private:
		vtkVisualProperty()
		{

		}
	};

	template<typename T>
	class vtkVisualProperty<T, 1> : public vtkVisualPropertyKey
	{	
	public:
		//ctor for unbounded properties
		vtkVisualProperty(const char* name, T defaultValue, const char* desc = nullptr) :
			vtkVisualPropertyKey(name, new vtkVisualPropertyDataTypeValue<T>(defaultValue), desc)
		{

		}

		//ctor for bounded properties		
		vtkVisualProperty(const char* name, T defaultValue, T minValue, T maxValue, const char* desc = nullptr) :
			vtkVisualPropertyKey(name, new vtkVisualPropertyDataTypeValue<T>(defaultValue), 
				new vtkVisualPropertyDataTypeValue<T>(minValue), new vtkVisualPropertyDataTypeValue<T>(maxValue), desc)
		{

		}

		//ctor for properties having only one boundary		
		vtkVisualProperty(const char* name, T defaultValue, T minOrMaxValue, bool isMinValue, const char* desc = nullptr) :
			vtkVisualPropertyKey(name, new vtkVisualPropertyDataTypeValue<T>(defaultValue),
				isMinValue ? new vtkVisualPropertyDataTypeValue<T>(minOrMaxValue) : nullptr,
				!isMinValue ? new vtkVisualPropertyDataTypeValue<T>(minOrMaxValue) : nullptr,
				desc)
		{

		}
	public:
		/** Gets the effective value vtkVisualPropertyValue in the property value of this property. 
		Returns the default value, if propVal is null. */
		T GetValue(const vtkVisualPropertyValue* propVal) const
		{
			auto o = dynamic_cast<const vtkVisualPropertyDataTypeValue<T>*>(propVal != nullptr ? &propVal->GetValue() : GetDefaultValue());
			return o->GetValue();
		}

		/** Sets the new effective value stored in the property value of this property. */
		void SetValue(vtkVisualPropertyValue* propVal, const T newValue) const
		{
			auto o = dynamic_cast<vtkVisualPropertyDataTypeValue<T>*>(&propVal->GetValue());
			return o->SetValue(newValue);
		}
	};

	template< >
	class vtkVisualProperty<bool, 1> : public vtkVisualPropertyKey
	{
	public:
		//ctor for unbounded properties
		vtkVisualProperty(const char* name, bool defaultValue, const char* desc = nullptr) :
			vtkVisualPropertyKey(name, new vtkVisualPropertyDataTypeValue<bool>(defaultValue), desc)
		{

		}
	public:
		/** Gets the effective value stored in the property value of this property.
		Returns the default value, if propVal is null. */
		bool GetValue(const vtkVisualPropertyValue* propVal) const
		{
			auto o = dynamic_cast<const vtkVisualPropertyDataTypeValue<bool>*>(propVal != nullptr ? &propVal->GetValue() : GetDefaultValue());
			return o->GetValue();
		}

		/** Sets the new effective value stored in the property value of this property. */
		void SetValue(vtkVisualPropertyValue* propVal, bool newValue) const
		{
			auto o = dynamic_cast<vtkVisualPropertyDataTypeValue<bool>*>(&propVal->GetValue());
			return o->SetValue(newValue);
		}
	};

	template<typename T>
	class vtkVisualProperty<T, 3> : public vtkVisualPropertyKey
	{
	public:
		vtkVisualProperty(const char* name, T defaultValue_C1, T defaultValue_C2, T defaultValue_C3, const char* desc = nullptr) :
			vtkVisualPropertyKey(name, new vtkVisualPropertyDataTypeVecValue<T, 3>(defaultValue_C1, defaultValue_C2, defaultValue_C3), desc)
		{

		}

		template<size_t N>
		vtkVisualProperty(const char* name,	const T(&defaultValue)[N], 
			const T(&minValue)[N], const T(&maxValue)[N], const char* desc = nullptr) :
			vtkVisualPropertyKey(name,				
				new vtkVisualPropertyDataTypeVecValue<T, 3>(defaultValue),
				new vtkVisualPropertyDataTypeVecValue<T, 3>(minValue),
				new vtkVisualPropertyDataTypeVecValue<T, 3>(maxValue), desc)
		{
			static_assert(N == 3, "defaultValue must have 3 elements.");	
		}

		template<size_t N>
		vtkVisualProperty(const char* name, const T(&defaultValue)[N],
			const T(&minOrMaxValue)[N], bool isMinValue, const char* desc = nullptr) :
			vtkVisualPropertyKey(name,
				new vtkVisualPropertyDataTypeVecValue<T, 3>(defaultValue),
				isMinValue ? new vtkVisualPropertyDataTypeVecValue<T, 3>(minOrMaxValue) : nullptr,
				!isMinValue ? new vtkVisualPropertyDataTypeVecValue<T, 3>(minOrMaxValue) : nullptr, desc)
		{
			static_assert(N == 3, "defaultValue must have 3 elements.");
		}

	public:
		/** Gets the effective value stored in the property value of this property.
		Returns the default value, if propVal is null. */
		const T* GetValue(const vtkVisualPropertyValue* propVal) const
		{
			auto o = dynamic_cast<const vtkVisualPropertyDataTypeVecValue<T,3>*>(propVal != nullptr ? &propVal->GetValue() : GetDefaultValue());
			return o->GetValue();
		}

		/** Sets the new effective value stored in the property value of this property. */		
		void SetValue(vtkVisualPropertyValue* propVal, const T(&newValue)[3]) const
		{
			auto o = dynamic_cast<vtkVisualPropertyDataTypeVecValue<T,3>*>(&propVal->GetValue());
			return o->SetValue(newValue);
		}
	};

	template < >
	class vtkVisualProperty<char*, 0> : public vtkVisualPropertyKey
	{
	public:
		vtkVisualProperty(const char* name, const char* defaultValue, const char* desc = nullptr) :
			vtkVisualPropertyKey(name, new vtkVisualPropertyDataTypeStringValue(defaultValue), desc)
		{

		}

	public:
		/** Gets the effective value stored in the property value of this property.
		Returns the default value, if propVal is null. */
		const char* GetValue(const vtkVisualPropertyValue* propVal) const
		{
			auto o = dynamic_cast<const vtkVisualPropertyDataTypeStringValue*>(propVal != nullptr ? &propVal->GetValue() : GetDefaultValue());
			return o->GetValue();
		}

		/** Sets the new effective value stored in the property value of this property. */
		void SetValue(vtkVisualPropertyValue* propVal, const char* newValue) const
		{
			auto o = dynamic_cast<vtkVisualPropertyDataTypeStringValue*>(&propVal->GetValue());
			return o->SetValue(newValue);
		}
	};

	template <typename T>
	using vtkVisualPropertyVector = vtkVisualProperty<T, 3> ;
	using vtkVisualPropertyString = vtkVisualProperty<char*, 0> ;	
}