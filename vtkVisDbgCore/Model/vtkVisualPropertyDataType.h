#pragma once

#include "../vtkVisDbgDefs.h"
#include <cstddef>

namespace vtkVisDbg
{
	enum class vtkVisualPropertyDataType
	{
		Void,
		Bool,	//true, false, represented by a checkbox
		Int32,	//32-bit number, represented by an editbox (with +/- buttons, if it has a limited range) or slider
		Int64,	//64-bit number, represented by an editbox (with +/- buttons, if it has a limited range) or slider
		Double,	//floating number, represented by an editbox or slider (if it has a limited range)
		String, //ANSI string
		Vec3Int32,	//int[3]
		Vec3Int64,	//long long[3]
		Vec3Double,	//double[3]	
	};

	namespace details
	{
		//--------------------------------
		// Identifies the internal data type of a visual property from the C++ data type
		template <typename T, size_t C = 1>
		struct Cpp2VisPropDataType
		{
			//"Unsupported data type for VisualProperty - see vtkVisualPropertyDataType.";
		};

		template <>
		struct Cpp2VisPropDataType<bool>
		{
			static const vtkVisualPropertyDataType visprop_type = vtkVisualPropertyDataType::Bool;
		};

		template <>
		struct Cpp2VisPropDataType<int>
		{
			static const vtkVisualPropertyDataType visprop_type = vtkVisualPropertyDataType::Int32;
		};

		template <>
		struct Cpp2VisPropDataType<long long>
		{
			static const vtkVisualPropertyDataType visprop_type = vtkVisualPropertyDataType::Int64;
		};

		template <>
		struct Cpp2VisPropDataType<double>
		{
			static const vtkVisualPropertyDataType visprop_type = vtkVisualPropertyDataType::Double;
		};

		template <>
		struct Cpp2VisPropDataType<int*, 3>
		{
			static const vtkVisualPropertyDataType visprop_type = vtkVisualPropertyDataType::Vec3Int32;
		};

		template <>
		struct Cpp2VisPropDataType<long long*, 3>
		{
			static const vtkVisualPropertyDataType visprop_type = vtkVisualPropertyDataType::Vec3Int64;
		};

		template <>
		struct Cpp2VisPropDataType<double*, 3>
		{
			static const vtkVisualPropertyDataType visprop_type = vtkVisualPropertyDataType::Vec3Double;
		};

		//--------------------------------
		// Compares values
		template < typename T1, typename T2 >
		int CompareValues(T1 val1, T2 val2)
		{
			if (val1 < val2)
				return -1;
			else if (val2 < val1)
				return 1;
			else
				return 0;
		}

		template < >
		constexpr int CompareValues<int, int>(int val1, int val2)
		{
			return val1 - val2;
		}

		template < >
		constexpr int CompareValues<long, long>(long val1, long val2)
		{
			return val1 - val2;
		}		
	}


	/**
	Represents the base class for storing the values of the visual propery datatype
	*/
	class vtkVisDbg_EXPORTS vtkVisualPropertyDataTypeVoidValue
	{
	public:
		vtkVisualPropertyDataTypeVoidValue()
		{
			m_Initialized = true;
			m_Modified = false;
		}

	public:
		/** Returns true, if the value was initialized (i.e., set) */
		bool IsInitialized() const {
			return m_Initialized;
		}

		/** Returns true, if the value has been modified since the last update. */
		bool IsModified() const {
			return m_Modified;
		}		

	public:
		/** Returns the data type of this value object */
		virtual vtkVisualPropertyDataType GetDataType() const {
			return vtkVisualPropertyDataType::Void;
		}

		/** Creates a new instance of the same object (uninitialized) */
		virtual vtkVisualPropertyDataTypeVoidValue* NewInstance() const {
			return new vtkVisualPropertyDataTypeVoidValue();
		}

		/** Creates a new instance of the same object (initialized to the current value) */
		virtual vtkVisualPropertyDataTypeVoidValue* Clone() const {
			return new vtkVisualPropertyDataTypeVoidValue();
		}

		/** 
		Compares the value in this object with the value stored in the other object.
		Exception: throws an exception, if data types differ
		Returns: value < 0, if this object precedes the other,
		0, if this object has the same position in the sort order as the other one,
		value > 0, this object follows the other
		Special rules for unset (not initialized) values (this - other):
		UnSet - Any value => < 0
		UnSet - UnSet => 0
		Any Value - UnSet => > 0
		*/
		virtual int CompareTo(const vtkVisualPropertyDataTypeVoidValue* other) const
		{			
			return 0;
		}

		/** Sets the value of this object to the value of the source. 
		Changes Initialized, Modified flags accordingly. */
		virtual void Copy(const vtkVisualPropertyDataTypeVoidValue* source)
		{
			//Nothing to DO, any value can be converted to void
		}

	protected:
		bool m_Initialized;	//!< True, if the value is initialized (and thus valid)
		bool m_Modified;	//!< True, if the value has been modified
	};

	template<typename T>
	class vtkVisualPropertyDataTypeValue : public vtkVisualPropertyDataTypeVoidValue
	{
	public:
		using value_type = T;

	public:
		vtkVisualPropertyDataTypeValue()
		{
			m_Initialized = false;
			m_Modified = false;
		}

		vtkVisualPropertyDataTypeValue(T initialValue) : m_Value(initialValue)
		{
			m_Initialized = true;
			m_Modified = false;
		}

	public:
		/** Returns the data type of this value object */
		/*virtual*/ vtkVisualPropertyDataType GetDataType() const override {
			return details::Cpp2VisPropDataType<T>::visprop_type;
		}

		/** Creates a new instance of the same object (uninitialized) */
		/*virtual*/ vtkVisualPropertyDataTypeVoidValue* NewInstance() const override {
			return new vtkVisualPropertyDataTypeValue<T>();
		}

		/** Creates a new instance of the same object (initialized to the current value) */
		/*virtual*/ vtkVisualPropertyDataTypeVoidValue* Clone() const override {
			return !IsInitialized() ? NewInstance() :
				new vtkVisualPropertyDataTypeValue<T>(m_Value);
		}

		/**
		Compares the value in this object with the value stored in the other object.
		Exception: throws an exception, if data types differ
		Returns: value < 0, if this object precedes the other,
		0, if this object has the same position in the sort order as the other one,
		value > 0, this object follows the other
		Special rules for unset (not initialized) values (this - other):
		UnSet - Any value => < 0
		UnSet - UnSet => 0
		Any Value - UnSet => > 0
		*/
		/*virtual*/ int CompareTo(const vtkVisualPropertyDataTypeVoidValue* other) const override
		{
			auto o = dynamic_cast<const vtkVisualPropertyDataTypeValue<T>*>(other);
			if (this->IsInitialized())
			{
				if (other->IsInitialized())
					return details::CompareValues(this->m_Value, o->m_Value);
				else
					return 1;
			}
			else if (other->IsInitialized())
				return -1;
			else
				return 0;
		}

		/** Sets the value of this object to the value of the source.
		Changes Initialized, Modified flags accordingly. */
		/*virtual*/ void Copy(const vtkVisualPropertyDataTypeVoidValue* source) override
		{
			int cmp = CompareTo(source);	//check, if the source differ
			if (cmp == 0) {
				return;		//no modification
			}

			auto o = dynamic_cast<const vtkVisualPropertyDataTypeValue<T>*>(source);			
			m_Value = o->m_Value;
			m_Initialized = o->m_Initialized;
			m_Modified = true;
		}

		/** Gets the native value. 
		N.B. the value might be uninitialized! */
		T GetValue() const
		{
			return m_Value;
		}

		/** Sets the new native value. */
		void SetValue(T newValue)
		{
			if (!IsInitialized() || m_Value != newValue)
			{
				m_Value = newValue;
				m_Initialized = true;
				m_Modified = true;
			}
		}

	protected:
		T m_Value;	//!< Container storing the data
	};
	

	template<typename T, size_t C, class Derived>
	class vtkVisualPropertyDataTypeVecValueTraits : public vtkVisualPropertyDataTypeVoidValue
	{
	public:
		using value_type = T;

	public:
		vtkVisualPropertyDataTypeVecValueTraits()
		{
			m_Initialized = false;
			m_Modified = false;
		}		

		template<size_t N>
		vtkVisualPropertyDataTypeVecValueTraits(const T(&initData)[N])
		{
			static_assert(N == C, "The vector must be initialized by exactly 'C' values.");

			for (size_t i = 0; i < C; i++) {
				m_Value[i] = initData[i];
			}

			m_Initialized = true;
			m_Modified = false;
		}

	public:
		/** Returns the data type of this value object */
		/*virtual*/ vtkVisualPropertyDataType GetDataType() const override {
			return details::Cpp2VisPropDataType<T*, C>::visprop_type;
		}

		/** Creates a new instance of the same object (uninitialized) */
		/*virtual*/ vtkVisualPropertyDataTypeVoidValue* NewInstance() const override {
			return new Derived();
		}

		/** Creates a new instance of the same object (initialized to the current value) */
		/*virtual*/ vtkVisualPropertyDataTypeVoidValue* Clone() const override {
			return !IsInitialized() ? NewInstance() :
				new Derived(m_Value);
		}

		/**
		Compares the value in this object with the value stored in the other object.
		Exception: throws an exception, if data types differ
		Returns: value < 0, if this object precedes the other,
		0, if this object has the same position in the sort order as the other one,
		value > 0, this object follows the other
		Special rules for unset (not initialized) values (this - other):
		UnSet - Any value => < 0
		UnSet - UnSet => 0
		Any Value - UnSet => > 0
		*/
		/*virtual*/ int CompareTo(const vtkVisualPropertyDataTypeVoidValue* other) const override
		{
			auto o = dynamic_cast<const Derived*>(other);
			if (o->IsInitialized()) {
				return InternalCompareTo(o->m_Value);
			}

			return this->IsInitialized() ? 1 : 0;
		}

		/** Sets the value of this object to the value of the source.
		Changes Initialized, Modified flags accordingly. */
		/*virtual*/ void Copy(const vtkVisualPropertyDataTypeVoidValue* source) override
		{
			int cmp = CompareTo(source);	//check, if the source differ
			if (cmp == 0) {
				return;		//no modification
			}
			
			if (m_Initialized = source->IsInitialized())
			{
				//we have some valid values to copy
				auto o = dynamic_cast<const Derived*>(source);
				for (size_t i = 0; i < C; i++) {
					m_Value[i] = o->m_Value[i];
				}
			}
			
			m_Modified = true;
		}

		/** Gets the native value.
		N.B. the value might be uninitialized! */
		const T* GetValue() const
		{
			return m_Value;
		}

		/** Sets the new native value. */
		template <size_t N>
		void SetValue(const T(&newData)[N])
		{
			static_assert(N == C, "The vector must be initialized by exactly 'C' values.");

			if (InternalCompareTo(newData) != 0) //data not the same
			{
				for (size_t i = 0; i < C; i++) {
					m_Value[i] = newData[i];
				}

				m_Initialized = true;
				m_Modified = true;
			}
		}

	private:
		int InternalCompareTo(const T(&newData)[C]) const
		{
			if (!this->IsInitialized())
				return -1;

			for (size_t i = 0; i < C; i++)
			{
				if (m_Value[i] != newData[i]) {
					return details::CompareValues(m_Value[i], newData[i]);
				}
			}

			return 0;
		}

	protected:
		T m_Value[C];	//!< Container storing the data
	};


	template<typename T, size_t C = 3>
	class vtkVisualPropertyDataTypeVecValue : public vtkVisualPropertyDataTypeVecValueTraits<T, C, vtkVisualPropertyDataTypeVecValue<T, C>>
	{
	public:
		vtkVisualPropertyDataTypeVecValue() : vtkVisualPropertyDataTypeVecValueTraits<T, C, vtkVisualPropertyDataTypeVecValue<T, C>>()
		{

		}

		template <size_t N>
		vtkVisualPropertyDataTypeVecValue(const T(&initData)[N]) : vtkVisualPropertyDataTypeVecValueTraits<T, C, vtkVisualPropertyDataTypeVecValue<T, C>>(initData)
		{
			static_assert(N == C, "The vector must be initialized by exactly 'C' values.");
		}
	};

	template<typename T>
	class vtkVisualPropertyDataTypeVecValue<T, 2> : public vtkVisualPropertyDataTypeVecValueTraits<T, 2, vtkVisualPropertyDataTypeVecValue<T, 2>>
	{
	public:
		vtkVisualPropertyDataTypeVecValue() : vtkVisualPropertyDataTypeVecValueTraits<T, 2, vtkVisualPropertyDataTypeVecValue<T, 2>>()
		{

		}

		template <size_t N>
		vtkVisualPropertyDataTypeVecValue(const T(&initData)[N]) : vtkVisualPropertyDataTypeVecValueTraits<T, 2, vtkVisualPropertyDataTypeVecValue<T, 2>>(initData)
		{
			static_assert(N == 2, "The vector must be initialized by exactly three values.");
		}

		vtkVisualPropertyDataTypeVecValue(const T el1, const T el2)
		{
			this->m_Value[0] = el1;
			this->m_Value[1] = el2;

			this->m_Initialized = true;
			this->m_Modified = false;
		}
	};

	template<typename T>
	class vtkVisualPropertyDataTypeVecValue<T, 3> : public vtkVisualPropertyDataTypeVecValueTraits< T, 3, vtkVisualPropertyDataTypeVecValue<T, 3>>
	{
	public:
		vtkVisualPropertyDataTypeVecValue() : vtkVisualPropertyDataTypeVecValueTraits<T, 3, vtkVisualPropertyDataTypeVecValue<T, 3>>()
		{

		}
		
		template <size_t N>
		vtkVisualPropertyDataTypeVecValue(const T(&initData)[N]) : vtkVisualPropertyDataTypeVecValueTraits<T, 3, vtkVisualPropertyDataTypeVecValue<T, 3>>(initData)
		{
			static_assert(N == 3, "The vector must be initialized by exactly three values.");
		}


		vtkVisualPropertyDataTypeVecValue(const T el1, const T el2, const T el3)
		{
			this->m_Value[0] = el1;
			this->m_Value[1] = el2;
			this->m_Value[2] = el3;

			this->m_Initialized = true;
			this->m_Modified = false;
		}
	};

	/**
	Represents the class for storing the string values of the visual propery datatype
	*/
	class vtkVisDbg_EXPORTS vtkVisualPropertyDataTypeStringValue : public vtkVisualPropertyDataTypeVoidValue
	{
	protected:
		class StringImpl;	//PIMPL

	public:
		vtkVisualPropertyDataTypeStringValue()
		{
			m_Value = nullptr;
			m_Initialized = false;			
		}

		vtkVisualPropertyDataTypeStringValue(const char* initValue);
		vtkVisualPropertyDataTypeStringValue(const vtkVisualPropertyDataTypeStringValue& source);
		~vtkVisualPropertyDataTypeStringValue();
	public:
		/** Returns the data type of this value object */
		/*virtual*/ vtkVisualPropertyDataType GetDataType() const override {
			return vtkVisualPropertyDataType::String;
		}

		/** Creates a new instance of the same object (uninitialized) */
		/*virtual*/ vtkVisualPropertyDataTypeVoidValue* NewInstance() const override {
			return new vtkVisualPropertyDataTypeStringValue();
		}

		/** Creates a new instance of the same object (initialized to the current value) */
		/*virtual*/ vtkVisualPropertyDataTypeVoidValue* Clone() const override {
			return !IsInitialized() ? NewInstance() : new vtkVisualPropertyDataTypeStringValue(*this);
		}

		/**
		Compares the value in this object with the value stored in the other object.
		Exception: throws an exception, if data types differ
		Returns: value < 0, if this object precedes the other,
		0, if this object has the same position in the sort order as the other one,
		value > 0, this object follows the other
		Special rules for unset (not initialized) values (this - other):
		UnSet - Any value => < 0
		UnSet - UnSet => 0
		Any Value - UnSet => > 0
		*/
		/*virtual*/ int CompareTo(const vtkVisualPropertyDataTypeVoidValue* other) const override;
		
		/** Sets the value of this object to the value of the source.
		Changes Initialized, Modified flags accordingly. */
		/*virtual*/ void Copy(const vtkVisualPropertyDataTypeVoidValue* source) override;
		
		/** Gets the native value.
		N.B. the value might be uninitialized! */
		const char* GetValue() const;

		/** Sets the new native value. */		
		void SetValue(const char* newString);
		

	protected:
		StringImpl* m_Value;
	};
}
