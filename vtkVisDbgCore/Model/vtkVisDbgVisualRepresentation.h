#pragma once

#include "../vtkVisualDebugger.h"
#include "vtkActorCollection.h"

namespace vtkVisDbg
{
	/// <summary>
	/// Abstract class of a single visual representation of a visual item.
	/// </summary>
	/// <remarks>
	/// An item(e.g., polygonal mesh) can be visualized using various visualization techniques,
	/// e.g., as bounding box, point cloud, wireframe, surface mesh, etc.Each of these techniques
	/// is one concrete implementation of vtkVisDbgVisualRepresentation.It has its properties that
	/// influences the final appearance.
	/// </remarks>
	class vtkVisDbg_EXPORTS vtkVisDbgVisualRepresentation : public vtkObject
	{
	public:


	};
}