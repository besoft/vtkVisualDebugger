#include "vtkObjectFactory.h"
#include "vtkVisualObject.h"
#include "../Internals/vtkVisualObjectInternal.h"
#include "../Internals/vtkVisualPropertyRegistrator.h"

#include <atomic>
namespace vtkVisDbg
{		
	CONSTRUCT_VISUALPROPERTY(vtkVisualObject, Visible, true, "Toggles the visibility of the object.");
	CONSTRUCT_VISUALPROPERTY(vtkVisualObject, Selected, false, "Selects the object in the navigation.");
	CONSTRUCT_VISUALPROPERTY(vtkVisualObject, Collapsed, true, "Hides the children in the navigation.");

	BEGIN_VISUALPROPERTY_REGISTRATION(vtkVisualObject)
		REGISTER_VISUALPROPERTY(vtkVisualObject, Visible);
		REGISTER_VISUALPROPERTY(vtkVisualObject, Selected);
		REGISTER_VISUALPROPERTY(vtkVisualObject, Collapsed);
	END_VISUALPROPERTY_REGISTRATION(vtkVisualObject)


	vtkStandardNewMacro(vtkVisualObject);	

	vtkVisualObject::vtkVisualObject()
	{
		static std::atomic<decltype(m_Id)> GlobalId(0);
		this->m_Id = GlobalId++;
		
#ifdef VTKVD_USE_PIMPL
		this->m_Internal = new vtkVisualObjectInternal();
#else
		this->m_Internal = &this->_InternalObj;
#endif		
	}

	vtkVisualObject::~vtkVisualObject()
	{
		RemoveAllChildren();
		
#ifdef VTKVD_USE_PIMPL
		delete this->m_Internal;
#endif		
	}
	

	//-------------------------------------------------------------------------
	// Adds a new child
	vtkIdType vtkVisualObject::AddChild(vtkVisualObject* child)
	{
		const vtkIdType index = FindChild(child);
		if (index >= 0)
			return index;

		//increase reference count of the child object
		//so that it cannot be deleted without calling RemoveChild	
		child->Register(this);
		this->m_Internal->m_Children.push_back(child);
	
		//as the child object cannot be deleted without RemoveChild,
		//which is called when this object is being deleted, 
		//it has a valid parent (this object) as long as it
		//is being a child of this object 
		//and thus increasing the reference count of this object 
		//is not needed (and would overcomplicated many things) 
		child->m_Internal->AddParent(this);
		return GetChildrenCount() - 1;
	}

	//-------------------------------------------------------------------------
	// Finds a child visual object
	vtkIdType vtkVisualObject::FindChild(vtkVisualObject* child) const
	{		
		const vtkIdType count = GetChildrenCount();
		for (vtkIdType i = 0; i < count; i++)
		{
			if (this->m_Internal->m_Children[i] == child)
				return i;
		}
		
		return -1;
	}

	//-------------------------------------------------------------------------
	// Removes the given visual object from the children collection
	void vtkVisualObject::RemoveChild(vtkVisualObject * child)
	{
		const vtkIdType index = FindChild(child);
		if (index >= 0) {
			RemoveChild(index);
		}
	}

	//-------------------------------------------------------------------------
	// Removes the visual object from the children collection
	void vtkVisualObject::RemoveChild(int index)
	{
		vtkVisualObject* obj = this->m_Internal->m_Children[index];
		this->m_Internal->m_Children.erase(this->m_Internal->m_Children.cbegin() + index);

		//remove ourselves from the parent list of the child visual object
		obj->m_Internal->RemoveParent(this);
		obj->UnRegister(this);		
	}
	
	//-------------------------------------------------------------------------
	// Removes all child visual objects
	void vtkVisualObject::RemoveAllChildren()
	{
		vtkIdType nCount = GetChildrenCount();
		while ((--nCount) >= 0)		
		{
			RemoveChild(nCount);
		}
	}


	//-------------------------------------------------------------------------
	// Gets the property key
	const vtkVisualPropertyKey* vtkVisualObject::GetVisualPropertyKey(const char* propertyKeyName) const
	{		
		std::type_index classType(typeid(*this));

		return vtkVisualPropertyRegistrator::GetVisualPropertyRegistrator()->FindKey(propertyKeyName, classType);
	}
}

#ifdef VTKVD_USE_PIMPL
#include "vtkVisualObject.inl"
#endif