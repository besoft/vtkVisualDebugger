#include "vtkVisualProperty.h"
#include <atomic>
#include <cassert>

namespace vtkVisDbg
{	
	vtkVisualPropertyKey::vtkVisualPropertyKey(const char* name, 
		vtkVisualPropertyDataTypeVoidValue* defaultValue,
		vtkVisualPropertyDataTypeVoidValue* minValue,
		vtkVisualPropertyDataTypeVoidValue* maxValue,
		const char* desc
	)
	{
		static std::atomic<decltype(m_GlobalIndex)> globalIndex;
		m_GlobalIndex = globalIndex++;

		m_Name = name;
		m_Description = desc;
		m_DefaultValue = defaultValue;
		m_MinValue = minValue;
		m_MaxValue = maxValue;

		assert(m_MinValue == nullptr || m_MinValue->GetDataType() == this->GetDataType());
		assert(m_MaxValue == nullptr || m_MaxValue->GetDataType() == this->GetDataType());
	}

	vtkVisualPropertyKey::~vtkVisualPropertyKey()
	{
		delete m_DefaultValue;
		delete m_MinValue;
		delete m_MaxValue;
	}

	void vtkVisualPropertyKey::SetDefaultValue(vtkVisualPropertyDataTypeVoidValue* newDefaultValue)
	{
		assert(newDefaultValue != nullptr);
		assert(newDefaultValue->GetDataType() == this->GetDataType());

		delete m_DefaultValue;
		m_DefaultValue = newDefaultValue;
	}

	void vtkVisualPropertyKey::SetMinValue(vtkVisualPropertyDataTypeVoidValue* newValue)
	{
		assert(newValue == nullptr || newValue->GetDataType() == this->GetDataType());

		delete m_MinValue;
		m_MinValue = newValue;
	}

	void vtkVisualPropertyKey::SetMaxValue(vtkVisualPropertyDataTypeVoidValue* newValue)
	{
		assert(newValue == nullptr || newValue->GetDataType() == this->GetDataType());

		delete m_MaxValue;
		m_MaxValue = newValue;
	}

	/** Creates a new instance of UnSet property value for this key. */
	/*virtual*/ vtkVisualPropertyValue* vtkVisualPropertyKey::CreatePropertyValue() const
	{		
		return new vtkVisualPropertyValue(this);
	}
}
