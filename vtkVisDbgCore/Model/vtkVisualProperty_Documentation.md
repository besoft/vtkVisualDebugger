# Visual Properties Documentation
Geometrical data, e.g., points in 3D, polygonial surface meshes, polylines, etc., optionally, with scalar or vector annotations, 
e.g., temperature measured in points, can be visualized using various techniques. Each of these techniques has typically various 
settings to customize the visualization.

A visual object represents a single piece of data being visualized by a concrete visualization technique. It is an instance 
of a *vtkVisualObject* subclass. For example, *vtkVisualPoints* visualizes points in the encapsulated data as colour glyphs, 
e.g., spheres or cubes, whereas the radius of these glyphs, shape, and concrete colour can be specified by the user.
This specification/configuration is done via visual properties, declared in the class of the visual object.

A visual property has

* internal (unique) identifier,
* name, e.g., "Size",
* data type of its value,
* effective (current) value,
* and metadata such as default value, min and max allowed value, etc.

Although a visual object may have lot of visual properties, typically only some of them holds values different from their defaults.
Hence, following the concept used in Microsoft WPF (see *DependencyProperty*), the storage of the effective (current) value is 
separated from the storage of the rest, which is static as it is common to all instances of the same class. For example, 
*vtkVisualPoints* declares visual properties "Size" and "CubeGlyph" with all its data/metadata being initialized statically, i.e., 
there is no overhead associated with instantiating. Only when the effective value of "Size" for an instance is specified, memory is allocated
(and reference is kept in this instance).

## vtkVisualPropertyDataType
Describes the data type of the effective, default, min and max value of a visual property.

## vtkVisualPropertyDataTypeVoidValue, vtkVisualPropertyDataTypeValue\<T\>, vtkVisualPropertyDataTypeVecValue\<T,C\>, vtkVisualPropertyDataTypeStringValue
Data storage for value of void, scalar (e.g., int, long, double, bool), vector (e.g., double\*), and string 
(i.e., zero terminated char\*) data types (see *vtkVisualPropertyDataType*). The value may be UNSET (not INITIALIZED). Provides methods Clone and CompareTo as well
as the methods to Get/Set the value. *vtkVisualPropertyDataTypeVoidValue* serves as a base class. Effective, default, min and max
values of visual properties are instances of *vtkVisualPropertyDataTypeVoidValue* subclass.

## vtkVisualPropertyKey, vtkVisualProperty\<T,C\>, vtkVisualPropertyVector\<T\>, vtkVisualPropertyString
The static part of the visual property storage. *vtkVisualPropertyKey* serves as a base class.

## vtkVisualPropertyValue
The effective part of the visual property storage. It contains the reference to the static part, i.e., to an instance of 
*vtkVisualPropertyKey* subclass and also the reference to an instance of the effective value (i.e., an instance of 
*vtkVisualPropertyDataTypeVoidValue* subclass).

