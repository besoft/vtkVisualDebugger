#pragma once

#include "vtkVisualPropertyDataType.h"
#include <string>


namespace vtkVisDbg
{
	class vtkVisualPropertyDataTypeStringValue::StringImpl : public std::string
	{
	public:
		StringImpl()
		{

		}

		StringImpl(const char* src) : std::string(src)
		{

		}

		StringImpl(const std::string& right) : std::string(right)
		{

		}

		StringImpl(std::string&& right) : std::string(right)
		{

		}
	};

	vtkVisualPropertyDataTypeStringValue::vtkVisualPropertyDataTypeStringValue(const char* initValue)
	{
		m_Value = new StringImpl(initValue);		
	}

	vtkVisualPropertyDataTypeStringValue::vtkVisualPropertyDataTypeStringValue(const vtkVisualPropertyDataTypeStringValue& source)
	{
		if (m_Initialized = source.IsInitialized()) {
			m_Value = new StringImpl(*source.m_Value);			
		}					
	}

	vtkVisualPropertyDataTypeStringValue::~vtkVisualPropertyDataTypeStringValue()
	{
		delete m_Value;
	}

	int vtkVisualPropertyDataTypeStringValue::CompareTo(const vtkVisualPropertyDataTypeVoidValue* other) const
	{
		auto o = dynamic_cast<const vtkVisualPropertyDataTypeStringValue*>(other);
		if (this->IsInitialized())
		{
			if (o->IsInitialized())
				return m_Value->compare(*o->m_Value);
			else
				return 1;
		}
		else if (o->IsInitialized())
			return -1;
		else
			return 0;		
	}

	void vtkVisualPropertyDataTypeStringValue::Copy(const vtkVisualPropertyDataTypeVoidValue* source)
	{
		int cmp = CompareTo(source);	//check, if the source differ
		if (cmp == 0) {
			return;		//no modification
		}

		delete m_Value;

		if (!(m_Initialized = source->IsInitialized()))
		{
			//source is not initialized but we are
			delete m_Value;
			m_Value = nullptr;
		}
		else
		{
			//we have some valid values to copy
			auto o = dynamic_cast<const vtkVisualPropertyDataTypeStringValue*>(source);
			m_Value = new StringImpl(*o->m_Value);
		}

		m_Modified = true;
	}
	
	const char* vtkVisualPropertyDataTypeStringValue::GetValue() const
	{
		return m_Value != nullptr ? m_Value->c_str() : nullptr;
	}

	/** Sets the new native value. */
	void vtkVisualPropertyDataTypeStringValue::SetValue(const char* newString)
	{
		if (!IsInitialized()) 
		{
			m_Value = new StringImpl(newString);
			m_Initialized = true;
			m_Modified = true;		
		}
		else if (m_Value->compare(newString) != 0)
		{
			*m_Value = newString;			
			m_Modified = true;
		}
	}
}