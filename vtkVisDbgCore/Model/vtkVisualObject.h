#pragma once

#include "../vtkVisDbgDefs.h"
#include "vtkVisualProperty.h"
#include "vtkObject.h"

#ifndef VTKVD_USE_PIMPL
#include "../Internals/vtkVisualObjectInternal.h"
#endif

namespace vtkVisDbg
{
#ifdef VTKVD_USE_PIMPL
	class vtkVisualObjectInternal;
#endif
	class vtkVisualPropertyRegistrator;

	/** 
	* @class vtkVisualObject
	* @brief Represents an object that participates in the visual debugging system.
	* 
	* A visual object contains the visual pipeline to visualize its associated data 
	* according to its settings present in visual properties. Visual objects form 
	* a rooted acyclic graph that is typically navigated and modified by the user 
	* via some GUI. An example of such graph is:
	* @verbatim
	*	Root (O) --- Group A (O) --- InGrA (C1 <- O) --- Obj1 (O)	
	*							 --\
	*								 Shared (C2 <- O) --- MyObj (C1<-O) 
	*			 --- Group B (O) --/
	*						 	 --- InGrB (C1 <- O)
	* @endverbatim
	* where the classes are given in brackets. When the graph should be visualized, 
	* the visual objects are processed in the depth first order starting from the 
	* root and the effective values of their visual properties are determined.
	* A visual property (e.g. Color) of a visual object may have its value unset,
	* set privately, or set to propagate to all the descendants of the object with
	* or without override ability. 
	* 	
	* Example: 
	* Group A sets its Color to green and set a flag that this value should be propagated 
	* without override, Group B to red privately, Shared to blue, and MyObj to yellow. 
	* All other visual objects leave the value unset. The algorithm for the determination 
	* of the effective values of the Color property works as follows. Starting with an empty 
	* stack S, it first visits the root. Root will be black, which is the default color of 
	* the Color visual property, because S is empty and Root has no value set. S remains empty
	* and, therefore, Group A will be green. Green is pushed to the stack together with its
	* flags. InGrA will be green because S.top is green and no value is set for InGrA. 
	* Obj1 will be yellow because the green at S.top does not have override flag.
	* Next is Shared. It has multiple parents and, therefore, its processing will be
	* deferred and S.top will be saved. Green is popped out from the stack.
	* Group B will be red. Next is Shared. Again, it will be deferred with the current
	* stack content. InGrB will be black (S is empty and no value is set). After that
	* the deferred nodes are processed. The stored values have to be merged. 
	* If there is any value with the override flag = true, the values with 
	* override = false are ignored and do not participate in the merging.
	* If all the values are the same, the merged value is this value and 
	* the override flag is set to the result of the logical OR operator applied 
	* to all override flags. If the values differ but the result of the logical 
	* OR operator is false and at the same time, the value of the deferred node 
	* is set and there are no descendants of that node or the value has override 
	* flag = true, the result of the merge is NULL (parents do not have any
	* impact). Otherwise, there a conflict is detected and the specialized method
	* defined for this visual property is called to resolve this conflict. The method
	* may simply prioritize some values, e.g., Visible will be true, average them,
	* etc. possibly taking into account the effective values of other visual
	* properties. For an example, if Visible is false, its value is ignored.	
	* 
	* OVERRIDE flag will not be realised, too complex, time consuming 
	* with a little reason to do so, if a value should be propagated, i.e., this is static inheritance
	*/
	class vtkVisDbg_EXPORTS vtkVisualObject : public vtkObject
	{
	public:
		vtkTypeMacro(vtkVisualObject, vtkObject);

	protected:
		/** Default constructor */
		vtkVisualObject();

		/** Destructor */
		~vtkVisualObject() override;

	public:
		/** Creates an instance of the object */
		static vtkVisualObject *New();

#pragma region Id
		/**
		Gets the unique identifier of this visual object.
		@return	The identifier.
		*/
		inline vtkIdType GetId() {
			return m_Id;
		}

		/**
		Gets the name of this visual object.
		@return	The name.
		*/
		VTKVD_PIMPL_INLINE const char* GetName() const;

		/**
		Set the name of this visual object.
		@param	name	The new name.
		*/
		void SetName(const char* name);				
#pragma endregion 

#pragma region Hierarchy
		/**
		Query if this visual object has parent
		@return	True if it has a parent, false if not.
		*/
		inline bool HasParent() const {
			return GetParentsCount() > 0;
		}

		/**
		Query if this visual object has some children
		@return	True if it has some children, false if not.
		*/
		inline bool HasChildren() const {
			return GetChildrenCount() > 0;
		}

		/**
		Gets the number of parents
		@return	The parents count.
		*/
		VTKVD_PIMPL_INLINE vtkIdType GetParentsCount() const;

		/**
		Gets the number of children
		@return	The children count.
		*/
		VTKVD_PIMPL_INLINE vtkIdType GetChildrenCount() const;

		/**
		Adds a new child
		@param [in,out]	child	The visual object to be added as a child. MAY not be NULL.
		@return	The index of the added child.
		*/
		vtkIdType AddChild(vtkVisualObject* child);

		/**
		Finds a child visual object
		@param [in,out]	child	visual object.
		@return	The index of the found child, -1 if not found.
		*/
		vtkIdType FindChild(vtkVisualObject* child) const;

		/**
		Removes the given visual object from the children collection
		@param [in,out]	child	If non-null, the child.
		*/
		void RemoveChild(vtkVisualObject* child);

		/**
		Removes the visual object from the children collection
		@param	index	Zero-based index of the child visual object to remove.
		*/
		void RemoveChild(int index);

		/** Removes all child visual objects. */
		void RemoveAllChildren();

		/**
		Gets a child visual object.
		@param	index	Zero-based index of the child visual object to retrieve.
		@return	Null if it fails, else the child.
		*/
		VTKVD_PIMPL_INLINE vtkVisualObject* GetChild(vtkIdType index) const;

		/**
		Gets a parent visual object.
		@param	index	Zero-based index of the parent visual object to retrieve.
		@return	Null if it fails, else the parent.
		*/
		VTKVD_PIMPL_INLINE vtkVisualObject* GetParent(vtkIdType index) const;
#pragma endregion 

#pragma region Properties
	public:
		static vtkVisualProperty<bool> Visible;		//!< Set to true (default), if this visual object is visible
		static vtkVisualProperty<bool> Selected;	//!< Set to false (default), if this visual object is selected in the navigation
		static vtkVisualProperty<bool> Collapsed;	//!< Set to false (default), if the children of this visual object should not be displayed in the navigation	

		//------------------------------------------------
		// Methods for direct manipulation with the values of visual properties
		
		/** Gets the effective, inherited, or default value of the specified visual property. */ 
		template <typename T>
		const T GetValue(const vtkVisualProperty<T, 1>& prop) const
		{
			return prop.GetValue(GetVisualPropertyValue(prop));
		}

		template <typename T>
		const T* GetValue(const vtkVisualPropertyVector<T>& prop) const
		{
			return prop.GetValue(GetVisualPropertyValue(prop));
		}
		
		const char* GetValue(const vtkVisualPropertyString& prop) const
		{
			return prop.GetValue(GetVisualPropertyValue(prop));
		}
		
		/**
		Gets the property value.
		@param	prop	The visual property.
		@return	Null if it fails, else the property value.

		Returns the effective value of the visual property or if it does not exist,
		the value inherited from the parents of this visual object, or if no such value
		exists, it returns null. */
		const vtkVisualPropertyValue* GetVisualPropertyValue(const vtkVisualPropertyKey& prop) const;

		
		/** Sets the effective value of the specified visual property. */
		template <typename T>
		void SetValue(const vtkVisualProperty<T, 1>& prop, const T value)
		{
			return prop.SetValue(CreateVisualPropertyValue(prop), value);
		}

		template <typename T>
		void SetValue(const vtkVisualPropertyVector<T>& prop, const T* value)
		{
			return prop.SetValue(CreateVisualPropertyValue(prop), value);
		}

		void SetValue(const vtkVisualPropertyString& prop, const char* value)
		{
			return prop.SetValue(CreateVisualPropertyValue(prop), value);
		}

		/**
		Creates a new effective value for the specified visual property.

		Returns the effective value of the visual property. */
		vtkVisualPropertyValue* CreateVisualPropertyValue(const vtkVisualPropertyKey& prop);


		/**
		Gets the property key

		N.B. The method is ineffective.
		@param	propertyKeyName	Name of the property key.
		@return	Null if it fails, else the property key.
		*/
		const vtkVisualPropertyKey* GetVisualPropertyKey(const char* propertyKeyName) const;
#pragma endregion 

	protected:
		vtkVisualObjectInternal * m_Internal;   //!< PIMPL mechanism to avoid DLLs problems due to STD		
#ifndef VTKVD_USE_PIMPL
	private:
		vtkVisualObjectInternal _InternalObj;  //!< PIMPL aggregated to support inlining of methods for statically linked libraries
#endif
	protected:
		vtkIdType m_Id; //!< The identifier		

	private:
		vtkVisualObject(const vtkVisualObject&) = delete;			// Not implemented.
		void operator = (const vtkVisualObject&) = delete;		// Not implemented.  
	};
}

#ifndef VTKVD_USE_PIMPL
#include "vtkVisualObject.inl"
#endif
