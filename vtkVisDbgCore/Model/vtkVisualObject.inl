#include "vtkVisualObject.h"
namespace vtkVisDbg
{	
	//-------------------------------------------------------------------------
	// Gets the name of this visual object
	VTKVD_PIMPL_INLINE const char* vtkVisualObject::GetName() const
	{
		return this->m_Internal->m_Name.c_str();
	}

	VTKVD_PIMPL_INLINE void vtkVisualObject::SetName(const char * name)
	{
		if (name == nullptr) {
			name = "";
		}

		if (this->m_Internal->m_Name != name)
		{
			this->m_Internal->m_Name = name;
			this->Modified();
		}
	}

	//-------------------------------------------------------------------------
	// Gets the number of parents
	VTKVD_PIMPL_INLINE vtkIdType vtkVisualObject::GetParentsCount() const
	{
		return static_cast<vtkIdType>(this->m_Internal->m_Parents.size());
	}

	//-------------------------------------------------------------------------
	// Gets the number of children
	VTKVD_PIMPL_INLINE vtkIdType vtkVisualObject::GetChildrenCount() const
	{
		return static_cast<vtkIdType>(this->m_Internal->m_Children.size());
	}	

	//-------------------------------------------------------------------------
	// Gets a child visual object.
	VTKVD_PIMPL_INLINE vtkVisualObject* vtkVisualObject::GetChild(vtkIdType index) const
	{
		return this->m_Internal->m_Children[index];
	}

	//-------------------------------------------------------------------------
	// Gets a parent visual object.
	VTKVD_PIMPL_INLINE vtkVisualObject* vtkVisualObject::GetParent(vtkIdType index) const
	{
		return this->m_Internal->m_Parents[index];
	}


	//-------------------------------------------------------------------------
	// Gets the property value.	
	VTKVD_PIMPL_INLINE const vtkVisualPropertyValue* vtkVisualObject::GetVisualPropertyValue(const vtkVisualPropertyKey& prop) const
	{
		auto retVal = m_Internal->GetPropertyEffectiveValue(prop);
		return retVal == nullptr ? m_Internal->GetPropertyInheritedValue(prop) : retVal;
	}

	//-------------------------------------------------------------------------
	//	Creates a new effective value for the specified visual property.
	//	Returns the effective value of the visual property.
	VTKVD_PIMPL_INLINE vtkVisualPropertyValue* vtkVisualObject::CreateVisualPropertyValue(const vtkVisualPropertyKey& prop)
	{
		return m_Internal->CreatePropertyEffectiveValue(prop);
	}
}
