#pragma once

//see: http://gernotklingler.com/blog/creating-using-shared-libraries-different-compilers-different-operating-systems/
//see also: https://cmake.org/cmake/help/v3.0/module/GenerateExportHeader.html
#include "vtkVisDbg_EXPORTS.h"

#ifdef SHARED_EXPORTS_BUILT_AS_STATIC
#define PIMPL_INLINE	inline
#else
#define VTKVD_USE_PIMPL
#define VTKVD_PIMPL_INLINE
#endif

// When defined, dynamic inheritance of the visual property values will be enabled instead of static inheritance. 
// With dynamic inheritance, accessing the value of a visual property always requires to traverse through
// the hierarchy of the visual objects up to the root, while with the static inheritance the traversal
// is stopped at the first object having the value specified explicitly
#define VTKVD_USE_DYNAMIC_INHERITANCE
