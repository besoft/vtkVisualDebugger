#pragma once

#include "vtkVisDbgDefs.h"
#include "vtkMAFVisualDebugger.h"
#include "Model/vtkVisualPropertyDataType.h"

namespace vtkVisDbg
{
	using vtkVisualDebugger = vtkMAFVisualDebugger;
}
