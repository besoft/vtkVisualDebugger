/*=========================================================================
Program: Multimod Application Framework RELOADED
Module: $RCSfile: vtkMAFVisualDebugger.cpp,v $
Language: C++
Date: $Date: 2009-05-19 14:30:01 $
Version: $Revision: 1.0 $
Authors: Josef Kohout, Jana H�jkov�
==========================================================================
Copyright (c) 2012 University of University of West Bohemia
See the COPYINGS file for license details
=========================================================================
*/
#include "vtkMAFVisualDebugger.h"
#include "Internals/vtkMAFVisualDebuggerInternal.h"
#include "vtkVersion.h"

#include "vtkObjectFactory.h"
#include "vtkPolyData.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkIdList.h"

#pragma warning(push)
#pragma warning(disable: 4996)
#include "vtkRenderer.h"
#include "vtkCubeSource.h"
#include "vtkRenderer.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkProperty.h"
#include "vtkSphereSource.h"
#include "vtkGlyph3D.h"
#include "vtkTubeFilter.h"
#include "vtkSmartPointer.h"
#include "vtkCellArray.h"
#include "vtkCamera.h"
#include "vtkCallbackCommand.h"
#include "vtkRendererCollection.h"
#include "vtkTextActor.h"
#include "vtkTextMapper.h"
#include "vtkTextProperty.h"
#include "vtkProperty2D.h"
#include "vtkDoubleArray.h"
#include "vtkScalarBarActor.h"
#include "vtkLookupTable.h"
#include "vtkPointData.h"
#include "vtkWindowToImageFilter.h"
#include "vtkPNGWriter.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkTransform.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkArrowSource.h"
#include "vtkMath.h"
#include "vtkCellPicker.h"
#include "vtkBillboardTextActor3D.h"
#pragma warning(pop)


	vtkStandardNewMacro(vtkMAFVisualDebugger);


	//define secure versions of C functions if available
#if defined(_MSC_VER) && _MSC_VER >= 1500
#define strcpy	strcpy_s
#define sprintf sprintf_s
#define strcat	strcat_s
#endif

///** initalize a given LUT with the idx.th preset */
//	extern void lutPreset(int idx, vtkLookupTable *lut);

	vtkMAFVisualDebugger::vtkMAFVisualDebugger()
	{
		m_NavigationState = vtkMAFVisualDebugger::NextStep;
		m_NumberOfIgnoredSteps = 5;
		m_nSkipIndex = -1;
		m_CurrentFrameId = -1;
		m_HelpDisplayed = false;

		WindowLeft = WindowTop = 0;
		WindowWidth = 1024;
		WindowHeight = 768;
		FullScreen = 1;
		BackgroundColor[0] = BackgroundColor[1] = BackgroundColor[2] = 1.0; //white
		ViewPersistency = false;
		ViewPersistencyFilename = nullptr;
		ScreenShotOnESC = false;
		ScreenShotOnESCFilename = nullptr;
		AutoPressESC = false;

		m_Renderer = vtkRenderer::New();
		m_RendererInteractor = NULL;			
		m_TotalResizeCoef = 1.0;

#ifdef VTKVD_USE_PIMPL
		this->m_Internal = new vtkMAFVisualDebuggerInternal();
#else
		this->m_Internal = &this->_InternalObj;
#endif

	}

	vtkMAFVisualDebugger::~vtkMAFVisualDebugger()
	{
		RemoveAll();	//remove all objects,
		//N.B, if RemoveAll is overriden, the inherited class must call RemoveAll in its destructor because
		//otherwise it won't be called (naturally)

		if (m_Renderer != NULL) {
			m_Renderer->Delete(); m_Renderer = NULL;
		}

		if (m_RendererInteractor != NULL) {
			m_RendererInteractor->Delete(); m_RendererInteractor = NULL;
		}

#ifdef VTKVD_USE_PIMPL
		delete this->m_Internal;
#endif
	}

	//------------------------------------------------------------------------
	//Removes all objects from the scene.
	/*virtual*/ void vtkMAFVisualDebugger::RemoveAll()
		//------------------------------------------------------------------------
	{
		while (!m_Internal->m_VisualObjects.empty()) {
			RemoveVisualObject(m_Internal->m_VisualObjects.begin()->first.c_str());
		}
	}

	//------------------------------------------------------------------------
	//Adds the new visual object into the scene or updates it, if the object already exists in the scene.
	//N.B. obj must be allocated on the heap and will be removed automatically when
	//corresponding RemoveVisualObject or	RemoveAll is called
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateVisualObject(const char* Id, vtkVisualObject_Old* obj)
		//------------------------------------------------------------------------
	{
		RemoveVisualObject(Id);	//make sure, the object does not exist

		for (int i = 0; i < obj->MAX_ACTORS; i++)
		{
			if (obj->pActors[i] != NULL)
			{
				obj->pActors[i]->SetPickable(0);	//set it not pickable to speed-up rendering
				m_Renderer->AddActor(obj->pActors[i]);
			}
		}

		m_Internal->m_VisualObjects[Id] = obj;
	}

	//------------------------------------------------------------------------
	//Removes the visual object from the scene.
	/*virtual*/ void vtkMAFVisualDebugger::RemoveVisualObject(const char* Id)
		//------------------------------------------------------------------------
	{
		auto it = m_Internal->m_VisualObjects.find(Id);
		if (it == m_Internal->m_VisualObjects.end())
			return;	//not found, so exit

		RemoveVisualObject(it->second);
		m_Internal->m_VisualObjects.erase(it);
	}


	//------------------------------------------------------------------------
	// Removes the visual object from the scene (without removing its reference).
	void vtkMAFVisualDebugger::RemoveVisualObject(vtkVisualObject_Old* obj)
	{
		for (int i = 0; i < obj->MAX_ACTORS; i++)
		{
			if (obj->pActors[i] != NULL) {
				m_Renderer->RemoveActor(obj->pActors[i]);
			}
		}

		if (obj->pResizableSource != nullptr) {
			obj->pResizableSource->Delete();
		}

		delete obj;	//free memory

	}

	//------------------------------------------------------------------------
	// Adds some label with the given Id from the scene.
	//If an object with the same Id already exists, it is updated.
	//The label is displayed using default font family and the given font size
	//in (r, g, b) colour at position x and y. Position is relative to the specified
	//horizontal and vertical alignment (parameters horiz_align and vert_align).
	//Valid values of alignment are -1, 0, 1 denoting LEFT, CENTER, RIGHT
	//or TOP, CENTER, BOTTOM alignment.
	/*virtual*/  void vtkMAFVisualDebugger::AddOrUpdateLabel(const char* Id,
		const char* label, int fontsize,
		double r, double g, double b,
		double x, double y,
		int horiz_align, int vert_align)
	{
		vtkVisualLabel* obj = new vtkVisualLabel();
		
		vtkSmartPointer < vtkTextActor > textActor = vtkTextActor::New();
		textActor->UnRegister(nullptr);
		 textActor->SetInput(label);

		textActor->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
		textActor->GetPositionCoordinate()->SetValue(x, y);
		textActor->GetProperty()->SetColor(r, g, b);

		textActor->GetTextProperty()->SetFontSize(fontsize);
		textActor->GetTextProperty()->SetJustification(horiz_align + 1); //see VTK_TEXT_LEFT - VTK_TEXT_RIGHT
		textActor->GetTextProperty()->SetVerticalJustification(-vert_align + 1); //see VTK_TEXT_TOP - VTK_TEXT_BOTTOM

		obj->pActors[0] = textActor.GetPointer();
		AddOrUpdateVisualObject(Id, obj);
	}

	//------------------------------------------------------------------------
	//Adds scalar field with the given Id into the scene.
	//If an object with the same Id already exists, it is updated.
	//Legend is displayed, if displaybar is true, in which case legend is shown
	//at x, y position relative to the window size and values of horiz_align and
	//vert_align - see also AddOrUpdateLabel method.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateScalarField(const char* Id,
		vtkPolyData* poly, bool displaybar,
		double x, double y, int horiz_align, int vert_align)
		//------------------------------------------------------------------------
	{
		double sr[2];
		poly->GetPointData()->GetScalars()->GetRange(sr);

		//build LUT
		vtkLookupTable* scalarFieldLUT = vtkLookupTable::New();
		scalarFieldLUT->SetNumberOfTableValues(256);
		for (int i = 0; i<256; i++)
		{
			double r, g, b;
			vtkMath::HSVToRGB((i*240.0) / 255.0 / 255.0, 1.0, 1.0, &r, &g, &b);
			scalarFieldLUT->SetTableValue(i, r, g, b, 1);
		}
		
		scalarFieldLUT->Build();

		scalarFieldLUT->SetTableRange(sr);

		vtkPolyDataMapper* mapper = vtkPolyDataMapper::New();
#if VTK_MAJOR_VERSION < 5
		mapper->SetInput(poly);
		mapper->ImmediateModeRenderingOn();
#else
		mapper->SetInputData(poly);		
#endif
		
		mapper->SetScalarModeToUsePointData();
		mapper->SetColorModeToMapScalars();
		mapper->SetLookupTable(scalarFieldLUT);
		mapper->SetScalarRange(sr);
		scalarFieldLUT->Delete();

		vtkSmartPointer< vtkActor > scalarActor = vtkActor::New();
		scalarActor->UnRegister(nullptr);
		scalarActor->SetMapper(mapper);

		vtkVisualScalarField* obj = new vtkVisualScalarField();
		obj->pActors[0] = scalarActor.GetPointer();

		vtkSmartPointer< vtkScalarBarActor> mapActor = vtkScalarBarActor::New();
		mapActor->UnRegister(nullptr);

		if (displaybar)
		{
			//we need to specify also legend
			mapActor = vtkScalarBarActor::New();
			mapActor->SetLookupTable(mapper->GetLookupTable());

			//calculate position
			double delta_x = 0.0, delta_y = 0.0;
			if (horiz_align == 0)
				delta_x = 0.5;
			else if (horiz_align > 0)
				delta_x = 1.0;

			if (vert_align == 0)
				delta_y = 0.5;
			else if (vert_align > 0)
				delta_y = 1.0;

			//specify position
			((vtkActor2D*)mapActor)->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
			((vtkActor2D*)mapActor)->GetPositionCoordinate()->SetValue(delta_x + x, delta_y + y);

			mapActor->SetOrientationToHorizontal();
			mapActor->SetWidth(0.8);
			mapActor->SetHeight(0.12);
			mapActor->SetLabelFormat("%6.0f");
			mapActor->SetPickable(0);   //make it faster

			//if window is too light, make our label darker so it is well visible
			if ((this->BackgroundColor[0] * 0.3 + this->BackgroundColor[1] * 0.6 + this->BackgroundColor[0] * 0.1) > 0.5)
				mapActor->GetProperty()->SetColor(0, 0, 0);
			obj->pActors[1] = mapActor.GetPointer();
		}

		mapper->Delete();

		AddOrUpdateVisualObject(Id, obj);
	}

	//------------------------------------------------------------------------
	//Adds surface mesh with the given Id into the scene.
	//If an object with the same Id already exists, it is updated.
	//(R,G,B) is colour in which the mesh is to be rendered.
	//Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateSurface(const char* Id,
		vtkPolyData* poly, double r, double g, double b,
		double opacity, bool wireframe)
		//------------------------------------------------------------------------
	{
		vtkPolyDataMapper* mapper = vtkPolyDataMapper::New();
#if VTK_MAJOR_VERSION < 5
		mapper->SetInput(poly);
#else
		mapper->SetInputData(poly);
#endif

		vtkSmartPointer< vtkActor > pointsActor = vtkActor::New();
		pointsActor->UnRegister(nullptr);
		pointsActor->SetMapper(mapper);
		mapper->Delete();

		pointsActor->GetProperty()->SetColor(r, g, b);
		pointsActor->GetProperty()->SetOpacity(opacity);

		if (wireframe)
			pointsActor->GetProperty()->SetRepresentationToWireframe();
/*
		pointsActor->GetProperty()->SetEdgeVisibility(true);
		pointsActor->GetProperty()->SetEdgeColor(1, 1, 0);
		pointsActor->GetProperty()->SetLineWidth(10);
		pointsActor->GetProperty()->RenderLinesAsTubesOn();


		pointsActor->GetProperty()->SetEdgeVisibility(true);
		//pointsActor->GetProperty()->(1, 1, 0);
		pointsActor->GetProperty()->SetPointSize(15);
		pointsActor->GetProperty()->RenderPointsAsSpheresOn();
*/

		vtkVisualSurface* obj = new vtkVisualSurface();
		obj->pActors[0] = pointsActor.GetPointer();
		AddOrUpdateVisualObject(Id, obj);
	}

	//------------------------------------------------------------------------
	//Adds points with the given Id into the scene.
	//If an object with the same Id already exists, it is updated.
	//Radius = radius of spheres to be rendered for the given points,
	//(R,G,B) is colour in which spheres are to be rendered.
	//Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdatePoints(const char* Id,
		vtkPolyData* points, double radius,
		double r, double g, double b,
		double opacity, bool wireframe, bool bcubeglyph)
		//------------------------------------------------------------------------
	{
		vtkGlyph3D* glfilter = vtkGlyph3D::New();
		vtkSphereSource* sphere = NULL;
		vtkCubeSource* cube = NULL;

		if (!bcubeglyph)
		{
			sphere = vtkSphereSource::New();

			double rd = radius * m_TotalResizeCoef;
			sphere->SetRadius(rd);

			int res = rd <= 1.0 ? 6 : (int)ceil(6*(1 + log10(sphere->GetRadius())));

			sphere->SetPhiResolution(res);
			sphere->SetThetaResolution(res);

#if VTK_MAJOR_VERSION < 5
			glfilter->SetSource(sphere->GetOutput());
#else
			glfilter->SetSourceConnection(sphere->GetOutputPort());
#endif
			
		}
		else
		{
			cube = vtkCubeSource::New();
			cube->SetXLength(radius);
			cube->SetYLength(radius);
			cube->SetZLength(radius);

#if VTK_MAJOR_VERSION < 5
			glfilter->SetSource(cube->GetOutput());
#else
			glfilter->SetSourceConnection(cube->GetOutputPort());
#endif
		}

#if VTK_MAJOR_VERSION < 5
		glfilter->SetInput(points);
#else
		glfilter->SetInputData(points);
		glfilter->Update();
#endif

		AddOrUpdateSurface(Id, glfilter->GetOutput(), r, g, b, opacity, wireframe);
		glfilter->Delete();

		vtkVisualObject_Old* obj = m_Internal->m_VisualObjects[Id];
		obj->pResizableSource = bcubeglyph ? vtkObject::SafeDownCast(cube) : vtkObject::SafeDownCast(sphere);		
	}

	//------------------------------------------------------------------------
	//Adds points with the given Id into the scene.
	//If an object with the same Id already exists, it is updated.
	//Radius = radius of spheres to be rendered for the given points,
	//(R,G,B) is colour in which spheres are to be rendered.
	//Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdatePoints(const char* Id,
		vtkPoints* points, double radius,
		double r, double g, double b,
		double opacity, bool wireframe, bool bCubeglyph)
		//------------------------------------------------------------------------
	{
		vtkPolyData* pointToInsert = vtkPolyData::New();
		pointToInsert->SetPoints(points);

		AddOrUpdatePoints(Id, pointToInsert, radius, r, g, b, opacity, wireframe, bCubeglyph);

		pointToInsert->Delete();
	}

	//------------------------------------------------------------------------
	//Adds points with the given Id into the scene.
	//If an object with the same Id already exists, it is updated.
	//Points = array of points in format (x1,y1,z1, x2, y2, z2, ...)
	//Count = number of points in points array
	//Radius = radius of spheres to be rendered for the given points,
	//(R,G,B) is colour in which spheres are to be rendered.
	//Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
	//If bCubeglyph is true, small cubes are rendered instead of spheres.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdatePoints(const char* Id,
		const double* points, int count, double radius,
		double r, double g, double b,
		double opacity, bool wireframe, bool bCubeglyph)
		//------------------------------------------------------------------------
	{
		vtkPoints* pp = vtkPoints::New(VTK_DOUBLE);
		pp->SetNumberOfPoints(count);
		for (int i = 0; i < count; i++) {
			pp->SetPoint(i, points + 3 * i);
		}

		AddOrUpdatePoints(Id, pp, radius, r, g, b, opacity, wireframe, bCubeglyph);

		pp->Delete();
	}

	//------------------------------------------------------------------------
	//Equivalent to AddOrUpdatePoints method with a small difference: this method visualize
	//only the points having their indices in validIndices array containing  validIndicesCount entries.
	//Note: validIndices can be easily retrieved from vtkIdList.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateSelectedPoints(const char* Id,
		vtkPolyData* points, const vtkIdType* validIndices,
		int validIndicesCount, double radius,
		double r, double g, double b, double opacity,
		bool wireframe, bool bCubeglyph)
	{
		AddOrUpdateSelectedPoints(Id, points->GetPoints(),
			validIndices, validIndicesCount, radius, r, g, b, opacity, wireframe, bCubeglyph);
	}

	//------------------------------------------------------------------------
	//Equivalent to AddOrUpdatePoints method with a small difference: this method visualize
	//only the points having their indices in validIndices array containing  validIndicesCount entries.
	//Note: validIndices can be easily retrieved from vtkIdList.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateSelectedPoints(const char* Id,
		vtkPoints* points, const vtkIdType* validIndices,
		int validIndicesCount, double radius,
		double r, double g, double b, double opacity, bool wireframe, bool bCubeglyph)
		//------------------------------------------------------------------------
	{
		vtkPoints* pp = vtkPoints::New();
		pp->SetNumberOfPoints(validIndicesCount);
		for (int i = 0; i < validIndicesCount; i++) {
			pp->SetPoint(i, points->GetPoint(validIndices[i]));
		}

		AddOrUpdatePoints(Id, pp, radius, r, g, b, opacity, wireframe, bCubeglyph);
		pp->Delete();
	}

	//------------------------------------------------------------------------
	//Equivalent to AddOrUpdatePoints method with a small difference: this method visualize
	//only the points having their indices in validIndices array containing  validIndicesCount entries.
	//Note: validIndices can be easily retrieved from vtkIdList.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateSelectedPoints(const char* Id,
		const double* points, int count, const vtkIdType* validIndices,
		int validIndicesCount, double radius,
		double r, double g, double b,
		double opacity, bool wireframe, bool bCubeglyph)
		//------------------------------------------------------------------------
	{
		vtkPoints* pp = vtkPoints::New();
		pp->SetNumberOfPoints(validIndicesCount);
		for (int i = 0; i < validIndicesCount; i++) {
			pp->SetPoint(i, points + 3 * validIndices[i]);
		}

		AddOrUpdatePoints(Id, pp, radius, r, g, b, opacity, wireframe, bCubeglyph);
		pp->Delete();
	}

	//------------------------------------------------------------------------
	// Adds lines with the given Id into the scene.
	//If an object with the same Id already exists, it is updated.
	//Radius = radius of tubes to be rendered for the given poly-lines,
	//(R,G,B) is colour in which tubes are to be rendered.
	//Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateLines(const char* Id,
		vtkPolyData* polylines, double radius,
		double r, double g, double b,
		double opacity, bool wireframe)
		//------------------------------------------------------------------------
	{
		vtkTubeFilter*  tube = vtkTubeFilter::New();
#if VTK_MAJOR_VERSION < 5
		tube->SetInput(polylines);
#else
		tube->SetInputData(polylines);
#endif
		tube->SetRadius(radius*m_TotalResizeCoef);
		tube->SetNumberOfSides(6);

#if VTK_MAJOR_VERSION >= 5
		tube->Update();
#endif
		AddOrUpdateSurface(Id, tube->GetOutput(), r, g, b, opacity, wireframe);

		vtkVisualObject_Old* obj = m_Internal->m_VisualObjects[Id];
		obj->pResizableSource = tube;		
	}

	//------------------------------------------------------------------------
	//Adds lines with the given Id into the scene.
	//If an object with the same Id already exists, it is updated.
	//PolyLine contains the connectivity of the poly-line, polyLineCount is number of entries.
	//Radius = radius of tubes to be rendered for the given poly-lines,
	//(R,G,B) is colour in which tubes are to be rendered.
	//Opacity is allows transparency, wireframe set to true enables rendering of wire frame model.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateLines(const char* Id,
		vtkPoints* points, const vtkIdType* polyLine, int polyLineCount,
		double radius, double r, double g, double b, double opacity, bool wireframe)
		//------------------------------------------------------------------------
	{
		vtkPolyData* poly = vtkPolyData::New();
		poly->SetPoints(points);

		vtkCellArray* cells = vtkCellArray::New();
		cells->Allocate(polyLineCount - 1);
		for (int i = 1; i < polyLineCount; i++)
		{
			vtkIdType clId[2] = { polyLine[i - 1], polyLine[i] };
			cells->InsertNextCell(2, clId);
		}

		poly->SetLines(cells);
		cells->Delete();

		AddOrUpdateLines(Id, poly, radius, r, g, b, opacity, wireframe);
		poly->Delete();
	}

	//------------------------------------------------------------------------
	// Adds a single axis of the given length.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateAxis(const char* Id, const double origin[3], const double dir[3],
		double tipRadius, double axisLength, double r, double g, double b)
		//------------------------------------------------------------------------
	{
		//the arrow is from (0,0,0) to (1,0,0), i.e., it lies on x-axis
		vtkSmartPointer< vtkArrowSource > arrow = vtkArrowSource::New();
		arrow->UnRegister(nullptr);

		//we want to rotate the axis so that it has the specified direction,
		//which actually means to rotate the coordinate system around y-axis
		//and around z-axis such that x-axis corresponds with dir direction
		vtkSmartPointer< vtkTransform > trans = vtkTransform::New();
		trans->UnRegister(nullptr);

		double dirN[3];
		for (int i = 0; i < 3; i++) {
			dirN[i] = dir[i];
		}
		vtkMath::Normalize(dirN);

		//first, rotate it around z axis	
#if VTK_MAJOR_VERSION < 5
		trans->RotateZ(atan2(dirN[1], dirN[0]) * vtkMath::RadiansToDegrees());
#else
		trans->RotateZ(vtkMath::DegreesFromRadians(atan2(dirN[1], dirN[0])));
#endif

		//now, rotate it around y axis		
#if VTK_MAJOR_VERSION < 5
		trans->RotateY(-atan2(dirN[2], sqrt(dirN[0] * dirN[0] + dirN[1] * dirN[1])) * vtkMath::RadiansToDegrees());
#else
		trans->RotateY(vtkMath::DegreesFromRadians(-atan2(dirN[2], sqrt(dirN[0] * dirN[0] + dirN[1] * dirN[1]))));
#endif

		trans->Scale(axisLength, axisLength, axisLength);

		trans->PostMultiply();
		trans->Translate(origin);

		//double pt_in[3] = { 1, 0, 0 }, pt_out[3];
		//trans->TransformPoint(pt_in, pt_out);

		//double pt_inh[4] = { 1, 0, 0,0 }, pt_outh[4];
		//trans->MultiplyPoint(pt_inh, pt_outh);

		vtkSmartPointer< vtkTransformPolyDataFilter > PDF = vtkTransformPolyDataFilter::New();
		PDF->UnRegister(nullptr);
		PDF->SetTransform(trans);
#if VTK_MAJOR_VERSION < 5
		PDF->SetInput(arrow->GetOutput());
#else
		PDF->SetInputConnection(arrow->GetOutputPort());
		PDF->Update();
#endif

		AddOrUpdateSurface(Id, PDF->GetOutput(), r, g, b);
	}

	//------------------------------------------------------------------------
	// Adds a local frame system.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateLocalFrame(const char* Id, vtkTransform* tr,
		double tipRadius, double axisLength, const double* originTr)
		//------------------------------------------------------------------------
	{
		AddOrUpdateLocalFrame(Id, tr->GetMatrix(), tipRadius, axisLength, originTr);
	}

	//------------------------------------------------------------------------
	// Adds a local frame system.
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateLocalFrame(const char* Id, vtkMatrix4x4* m,
		double tipRadius, double axisLength, const double* originTr)
		//------------------------------------------------------------------------
	{
		std::string prefix = Id;

		double O[4]{ 0,0,0, 1 };
		double origin[4];
		m->MultiplyPoint(O, origin);

		if (originTr == NULL)
			originTr = origin;

		double direction[4]{ 0, 0, 0, 1 };

		for (int i = 0; i < 3; i++)
		{
			direction[i] = 1;	//set the direction

			double dirTr[4];
			m->MultiplyPoint(direction, dirTr);
			for (int j = 0; j < 3; j++) {
				dirTr[j] -= origin[j];
			}

			AddOrUpdateAxis(((prefix + "_") + char('x' + i)).c_str(), originTr, dirTr, tipRadius, axisLength,
				direction[0], direction[1], direction[2]);

			direction[i] = 0;	//back to zeroes
		}
	}

	//------------------------------------------------------------------------
	/** Adds external actor with the given Id into the scene.
	If an object with the same Id already exists, it is updated. */
	/*virtual*/ void vtkMAFVisualDebugger::AddOrUpdateExternalObject(const char* Id, vtkProp* actor)
		//------------------------------------------------------------------------
	{
		vtkVisualObject_Old* obj = new vtkVisualObject_Old();
		obj->pActors[0] = actor;

		AddOrUpdateVisualObject(Id, obj);
	}

	//------------------------------------------------------------------------
	// Enables/Disables pickable ability of the visual object with the given Id.
	// N.B. Default is false to speed-up the rendering. 
	//------------------------------------------------------------------------
	void vtkMAFVisualDebugger::EnablePickable(const char* Id, bool enable)
	{
		auto it = m_Internal->m_VisualObjects.find(Id);
		if (it == m_Internal->m_VisualObjects.end())
			return;	//not found, so exit

		vtkVisualObject_Old* obj = it->second;
		if (obj->pActors[0] != NULL) {
			obj->pActors[0]->SetPickable(enable);
		}		
	}

	//------------------------------------------------------------------------
	// Creates the render window.
	vtkRenderWindow* vtkMAFVisualDebugger::CreateRenderWindow()
		//------------------------------------------------------------------------
	{
		vtkRenderWindow *renWin = vtkRenderWindow::New();
		m_Renderer->SetBackground(BackgroundColor);

		renWin->AddRenderer(m_Renderer);
		renWin->SetSize(WindowWidth, WindowHeight);
		renWin->SetPosition(WindowLeft, WindowTop);
		renWin->SetFullScreen(FullScreen);

		char szFrameId[20];
		sprintf(szFrameId, "Frame #%d", m_CurrentFrameId);
		renWin->SetWindowName(szFrameId);
		return renWin;
	}


	//----------------------------------------------------------------------------
	// Saves the current camera view
	void SaveView(const char* path, vtkRenderWindow* renWin)
	{
		FILE* f = NULL;
		if (0 != fopen_s(&f, path, "wb"))
			return;

		const int* winPosition = renWin->GetPosition();
		const int* winSize = renWin->GetSize();
		vtkTypeBool fs = renWin->GetFullScreen();

		fwrite(winPosition, sizeof(int), 2, f);
		fwrite(winSize, sizeof(int), 2, f);
		fwrite(&fs, sizeof(vtkTypeBool), 1, f);

		//get camera
		vtkCamera* cam = renWin->GetRenderers()
			->GetFirstRenderer()->GetActiveCamera();

		const double* pos = cam->GetPosition();
		fwrite(pos, sizeof(double), 3, f);

		pos = cam->GetFocalPoint();
		fwrite(pos, sizeof(double), 3, f);

		double val = cam->GetViewAngle();
		fwrite(&val, sizeof(double), 1, f);

		pos = cam->GetViewUp();
		fwrite(pos, sizeof(double), 3, f);

		val = cam->GetDistance();
		fwrite(&val, sizeof(double), 1, f);
		fclose(f);
	}

	//----------------------------------------------------------------------------
	// Restores the current camera view
	void RestoreView(const char* path, vtkRenderWindow* renWin)
	{
		FILE* f = NULL;
		if (0 != fopen_s(&f, path, "rb"))
			return;

		int winPosition[2], winSize[2];
		vtkTypeBool fs;

		fread(winPosition, sizeof(int), 2, f);
		fread(winSize, sizeof(int), 2, f);
		fread(&fs, sizeof(vtkTypeBool), 1, f);

		renWin->SetPosition(winPosition);
		renWin->SetSize(winSize);
		renWin->SetFullScreen(fs);

		//get camera
		vtkRenderer* ren1 = renWin->GetRenderers()->GetFirstRenderer();
		vtkCamera* cam = ren1->GetActiveCamera();

		double pos[3];
		fread(pos, sizeof(double), 3, f);
		cam->SetPosition(pos);

		fread(pos, sizeof(double), 3, f);
		cam->SetFocalPoint(pos);

		double val;
		fread(&val, sizeof(double), 1, f);
		cam->SetViewAngle(val);

		fread(pos, sizeof(double), 3, f);
		cam->SetViewUp(pos);

		fread(&val, sizeof(double), 1, f);
		cam->SetDistance(val);
		fclose(f);

		//and update everything	
		ren1->ResetCameraClippingRange();
	}

	//------------------------------------------------------------------------
	//Makes the screen shot of the render window and saves it to the specified file.
	void vtkMAFVisualDebugger::ScreenShot(const char* fileName, vtkRenderWindow* renWin)
		//------------------------------------------------------------------------
	{
		// Screenshot
		vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkWindowToImageFilter::New();
		windowToImageFilter->Delete();

		windowToImageFilter->SetInput(renWin);
		windowToImageFilter->Update();

		vtkSmartPointer<vtkPNGWriter> writer = vtkPNGWriter::New();
		writer->Delete();

		writer->SetFileName(fileName);
#if VTK_MAJOR_VERSION < 5
		writer->SetInput(windowToImageFilter->GetOutput());
#else
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
#endif
		writer->Write();
	}	

	//------------------------------------------------------------------------
	//Opens the rendering window displaying everything and interacting with the user.
	//The method ends when the user expresses his/her wishes to continue with the method being debugged.
	/*virtual*/ void vtkMAFVisualDebugger::DebugStep(bool keepRenderingWindow, std::function<void(void)> onTimerCallback)
		//------------------------------------------------------------------------
	{
		if (m_NavigationState == vtkMAFVisualDebugger::StopDebugging)
			return;	//there is nothing for us to do

		++m_CurrentFrameId;	//increase the number of frames

		if (m_NavigationState != vtkMAFVisualDebugger::NextStep) {
			m_NavigationState = (FrameNavigation)(m_NavigationState - 1); return;	//decrease the number of
		}

		vtkRenderWindow *renWin = NULL;
		vtkRenderWindowInteractor *iren = NULL;

		if (m_RendererInteractor != NULL)
		{
			iren = m_RendererInteractor;
			renWin = m_RendererInteractor->GetRenderWindow();

			iren->Render();
		}
		else
		{
			renWin = CreateRenderWindow();
			iren = vtkRenderWindowInteractor::New();
			iren->SetRenderWindow(renWin);
			renWin->Delete();

			vtkCallbackCommand *keypressCallback = vtkCallbackCommand::New();
			keypressCallback->SetClientData((void*)this);
			keypressCallback->SetCallback(&vtkMAFVisualDebugger::KeypressCallback);
			iren->AddObserver(vtkCommand::KeyPressEvent, keypressCallback);
			keypressCallback->Delete();

			vtkCellPicker* cellPicker = vtkCellPicker::New();
			vtkCallbackCommand* pickerCallback = vtkCallbackCommand::New();
			pickerCallback->SetClientData((void*)this);
			pickerCallback->SetCallback(&vtkMAFVisualDebugger::PickCallback);			
			cellPicker->AddObserver(vtkCommand::EndPickEvent, pickerCallback);
			iren->SetPicker(cellPicker);
			cellPicker->Delete();			
			
			vtkInteractorStyleTrackballCamera *style = vtkInteractorStyleTrackballCamera::New();
			iren->SetInteractorStyle(style);
			style->Delete();

			iren->Initialize();
		}

		if (ViewPersistency && ViewPersistencyFilename != nullptr) {
			RestoreView(ViewPersistencyFilename, renWin);
		}
		
		if (AutoPressESC || (m_TimerCallback = onTimerCallback) != nullptr)
		{
			vtkCallbackCommand *timerCallback = vtkCallbackCommand::New();
			timerCallback->SetClientData((void*)this);
			if (!AutoPressESC)
				timerCallback->SetCallback(&vtkMAFVisualDebugger::TimerCallback);
			else
				timerCallback->SetCallback([](vtkObject* caller, unsigned long eid, void* clientdata, void* calldata) 
					{
						//make it KeyDown ESC
						vtkRenderWindowInteractor* iren = ((vtkRenderWindowInteractor*)caller);
						iren->SetKeyCode((char)27);
						vtkMAFVisualDebugger::KeypressCallback(caller, eid, clientdata, calldata);
					}
			);
				
				//&vtkMAFVisualDebugger::TimerCallback);
			iren->AddObserver(vtkCommand::TimerEvent, timerCallback);
			timerCallback->Delete();

			iren->CreateTimer(VTKI_TIMER_FIRST);
		}

		m_NavigationState = FrameNavigation::StopDebugging;
		iren->Start();

		if (AutoPressESC || m_TimerCallback != nullptr)
		{
			iren->DestroyTimer();
			m_TimerCallback = nullptr;
		}

		//Keep changed staff
		WindowLeft = renWin->GetPosition()[0];
		WindowTop = renWin->GetPosition()[1];

		WindowWidth = renWin->GetSize()[0];
		WindowHeight = renWin->GetSize()[1];

		if (ViewPersistency && ViewPersistencyFilename != nullptr) {
			SaveView(ViewPersistencyFilename, renWin);
		}

		if (keepRenderingWindow && m_NavigationState != FrameNavigation::StopDebugging)
			m_RendererInteractor = iren;
		else
		{
			//Remove data
			iren->SetInteractorStyle(NULL);
			iren->Delete();

			m_RendererInteractor = nullptr;
		}
	}

	//------------------------------------------------------------------------
	/*static*/ void vtkMAFVisualDebugger::TimerCallback(vtkObject* caller,
		long unsigned int vtkNotUsed(eventId), void* clientData, void* vtkNotUsed(callData))
		//------------------------------------------------------------------------
	{
		vtkMAFVisualDebugger* pThis = (vtkMAFVisualDebugger*)clientData;
		pThis->m_TimerCallback();

		vtkRenderWindowInteractor* iren = ((vtkRenderWindowInteractor*)caller);
		iren->Render();	//ensure the window is updated
	}

	//------------------------------------------------------------------------
	/*static*/ void vtkMAFVisualDebugger::KeypressCallback(vtkObject* caller,
		long unsigned int vtkNotUsed(eventId), void* clientData, void* vtkNotUsed(callData))
		//------------------------------------------------------------------------
	{
		vtkMAFVisualDebugger* pThis = (vtkMAFVisualDebugger*)clientData;
		vtkRenderWindowInteractor* iren = ((vtkRenderWindowInteractor*)caller);

		char key = iren->GetKeyCode();
		if (key == char(27) || key == ' ') {  // if ESC or SPACE, close the window and continue in visualization
			pThis->m_NavigationState = vtkMAFVisualDebugger::NextStep;
			
			if (pThis->ScreenShotOnESC) {
				pThis->ScreenShot(pThis->ScreenShotOnESCFilename, iren->GetRenderWindow());
			}
			iren->ExitCallback();
		}
		else if (key == 'x') { //  if key X is pressed, visualization is stopped from the next step
			pThis->m_NavigationState = vtkMAFVisualDebugger::StopDebugging;
			iren->ExitCallback();
		}
		else if (key == '+' || key == '-') // change sizes of spheres, etc.
		{
			double dblCoef = key == '+' ? 2.0 : 0.5;

			pThis->m_TotalResizeCoef *= dblCoef;
			bool bHasChange = false;
			
			for (auto it = pThis->m_Internal->m_VisualObjects.begin(); it != pThis->m_Internal->m_VisualObjects.end(); ++it)
			{
				vtkSphereSource* sphere = vtkSphereSource::SafeDownCast(it->second->pResizableSource);
				if (sphere != NULL) 
				{
					sphere->SetRadius(sphere->GetRadius()*dblCoef); 
					it->second->pActors[0]->Modified();

					bHasChange = true;
				}

				vtkTubeFilter* tube = vtkTubeFilter::SafeDownCast(it->second->pResizableSource);
				if (tube != NULL) 
				{
					tube->SetRadius(tube->GetRadius()*dblCoef); 
					it->second->pActors[0]->Modified();

					bHasChange = true;					
				}
			}

			if (bHasChange)
			{
				vtkRenderWindow* win = iren->GetRenderWindow();

				win->Modified();
				win->Render();
			}
		}
		else if (key == 'k' || (key >= '0' && key <= '9') || key == char(13)) {	//skip some few steps
			if (pThis->m_nSkipIndex < 0 && key != 'k')
				return;	//ignore these keys because they come from invalid context

			if (key == 'k')	//we have start of entry
				pThis->m_nSkipIndex = 0;
			else if (key != (char)13)
			{
				//some character => let us get the next character, but avoid buffer overrun
				if (pThis->m_nSkipIndex < (sizeof(pThis->m_szSkip) / sizeof(char) - 1))
				{
					pThis->m_szSkip[pThis->m_nSkipIndex] = key;
					pThis->m_szSkip[++pThis->m_nSkipIndex] = '\0';
				}
			}
			else
			{
				//enter
				if (pThis->m_nSkipIndex != 0) {
					pThis->m_NumberOfIgnoredSteps = atoi(pThis->m_szSkip);
				}

				pThis->m_nSkipIndex = -1;	//close entering
				pThis->m_NavigationState = (FrameNavigation)pThis->m_NumberOfIgnoredSteps;

				pThis->RemoveLabel("__SKIPFRAMES");
				iren->ExitCallback();
				return;	//we are finish here
			}

			//OK, so display updated info for the user
			char szMessage[256];
			strcpy(szMessage, "Current number of frames to be skipped is ");

			char szVal[16];
			if (pThis->m_nSkipIndex == 0)
				sprintf(szVal, "%d", pThis->m_NumberOfIgnoredSteps);
			else
				strcpy(szVal, pThis->m_szSkip);

			strcat(szMessage, szVal);
			strcat(szMessage, ".\nPress <ENTER> to confirm or insert a new value.");

			pThis->AddOrUpdateLabel("__SKIPFRAMES", szMessage, 24,
				1.0, 0, 0,	//RED
				0.5, 0.85,			//Center position
				0, -1);			//left

			vtkRenderWindow* win = iren->GetRenderWindow();

			win->Modified();
			win->Render();
		}
		else
		{
			std::string keyN = iren->GetKeySym();
			if (keyN == "F1")
			{
				const char* helpString =
					"F1   = toggle this help\n"
					"F4   = print screen\n"
					"ESC  = close the window and continue the debugging (SPACE also works)\n"
					"k    = close the window and continue the debugging skipping some steps\n"
					"x    = stop visual debugging\n"
					"+/-  = increase/decrease size of points and lines\n"
					"r    = reset the camera view along the current view direction\n"
					"s    = switch to surface representation\n"
					"w    = switch to wireframe representation\n"
					"p    = pick the cell under the mouse cursor,\n"
					"       to hide annotation just pick empty space\n"
					"e/q  = terminate the application\n"
					;

				pThis->m_HelpDisplayed = !pThis->m_HelpDisplayed;	//toggle help
				if (pThis->m_HelpDisplayed)
					pThis->AddOrUpdateLabel("__HELPMSG", helpString, 12,
						0.5, 0.5, 0.5,	//RED
						0.05, 0.85,		//Center position
						-1, -1);		//centered in x and y
				else
					pThis->RemoveVisualObject("__HELPMSG");

				vtkRenderWindow* win = iren->GetRenderWindow();

				win->Modified();
				win->Render();
			}
			else if (keyN == "F4")
			{
				//Print screen
				char buffer[64];
				std::time_t t = std::time(nullptr);
#pragma warning(suppress:4996)
				auto lt = std::localtime(&t);

				sprintf(buffer, "%04d%02d%02d_%02d%02d%02d_Frame_#%d.png",
					1900 + lt->tm_year, lt->tm_mon + 1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec,
					pThis->m_CurrentFrameId
				);
				pThis->ScreenShot(buffer, iren->GetRenderWindow());
			}
		}
	}

	//------------------------------------------------------------------------
	/*static*/ void vtkMAFVisualDebugger::PickCallback(vtkObject* caller,
		long unsigned int vtkNotUsed(eventId), void* clientData, void* vtkNotUsed(callData))
		//------------------------------------------------------------------------
	{
		vtkMAFVisualDebugger* pThis = (vtkMAFVisualDebugger*)clientData;
		vtkCellPicker* picker = vtkCellPicker::SafeDownCast(caller);
		
		bool changed = false;

		auto cellId = picker->GetCellId();
		if (cellId < 0) 
		{
			//remove all picker annotations
			for (auto it = pThis->m_Internal->m_VisualObjects.begin(); it != pThis->m_Internal->m_VisualObjects.end();)
			{
				if (it->second->Kind != vtkVisualObject_Old::DataKind::PickAnnotation) {
					++it;
				}
				else
				{
					auto itNext = it;
					++itNext;
				
					pThis->RemoveVisualObject(it->second);
					pThis->m_Internal->m_VisualObjects.erase(it);

					it = itNext;
					changed = true;
				} 
			}
		}
		else
		{
			auto ds = picker->GetDataSet();
			auto cell = ds->GetCell(cellId);
			auto cellPtIds = cell->GetPointIds();
			auto cellPoints = cell->GetPoints();

			char id[64];
			sprintf_s(id, sizeof(id), "ID_PICKER_%p(%d", ds, (int)cellId);
			
			vtkNew<vtkPoints> points;
			vtkNew<vtkIdList> cellsIdList;
			
			auto nPoints = cellPoints->GetNumberOfPoints();
			for (vtkIdType i = 0; i < nPoints; i++)
			{
				vtkNew<vtkBillboardTextActor3D> textActor;
				textActor->SetPosition(cellPoints->GetPoint(i));
				textActor->GetTextProperty()->SetFontSize(12);
				textActor->GetTextProperty()->SetColor(0.0, 0.0, .4);
				textActor->GetTextProperty()->SetJustificationToCentered();

				double xyz[3];
				cellPoints->GetPoint(i, xyz);
				points->InsertNextPoint(xyz);
				
				char label[77];
				sprintf_s(label, sizeof(label), "#%d (%f ,%f, %f)", (int)cellPtIds->GetId(i), xyz[0], xyz[1], xyz[2]);
				textActor->SetInput(label);

				vtkVisualPickAnnotation* obj = new vtkVisualPickAnnotation();
				obj->pActors[0] = textActor;
				
				sprintf_s(label, sizeof(label), "%s:%d)", id, (int)i);
				pThis->AddOrUpdateVisualObject(label, obj);

				cellsIdList->InsertNextId(i);
			}

			vtkNew<vtkCellArray> cells;
			cells->InsertNextCell(cellsIdList);

			vtkNew<vtkPolyData> poly;
			poly->SetPoints(points);
			poly->SetPolys(cells);

			pThis->AddOrUpdateSurface(id, poly, 0,0,0, 1, true);
			auto it = pThis->m_Internal->m_VisualObjects.find(id);
			it->second->Kind = vtkVisualObject_Old::DataKind::PickAnnotation;
			changed = true;

			/*
			double xyz[3];
			picker->GetPickPosition(xyz);

			std::string label = std::to_string(xyz[0]) + ", " + std::to_string(xyz[1]) + ", " + std::to_string(xyz[0]);

			picker->GetSelectionPoint(xyz);
			pThis->AddOrUpdateLabel(IDpick, label.c_str(), 12,
				0, 0, 0,
				xyz[0] / pThis->m_Renderer->GetSize()[0],
				xyz[1] / pThis->m_Renderer->GetSize()[1],
				0, 0
			);
			*/				
		}		

		if (changed)
		{
			vtkRenderWindow* win = pThis->m_Renderer->GetRenderWindow();
			win->Modified();
			win->Render();
		}
	}
