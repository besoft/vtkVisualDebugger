#pragma once

#include "vtkVisualPropertyInternal.h"
#include <unordered_map>

namespace vtkVisDbg
{
	/**
	This class serves as a registrator of visual properties declared in visual objects.

	A visual property is declared in the class of a visual object as a static member field
	of vtkVisualProperty type, e.g., 
	
	\code{.cpp}
	class MyVisualObject : public vtkVisualObject {
		public:
			static vtkVisualProperty<bool> AnnotationEnabled;
			static vtkVisualPropertyVector<double> AnnotationColor;
	\endcode

	It must be constructed in .cpp file, either manually by calling the appropriate constructor,
	e.g., 


	\code{.cpp}	
	vtkVisualProperty<bool> MyVisualObject::AnnotationEnabled("AnnotationEnabled", false, "Toggless visibility of annotations");
	\endcode

	or by using a special macro CONSTRUCT_VISUALPROPERTY, which automatically stringify the property name, thus
	when the property is renamed, the value in the 'name' argument of the property constructor is consistent, e.g., 

	\code{.cpp}
	CONSTRUCT_VISUAL_PROPERTY(MyVisualObject, AnnotationColor, {0, 0, 0}, "Colour of annotations in RGB");
	\endcode

	Visual properties must be register in the system using vtkVisualPropertyRegistrator::RegisterVisualProperty
	method.	Typically, the programmer does not call this method directly but uses macros
	BEGIN_VISUALPROPERTY_REGISTRATION, REGISTER_VISUALPROPERTY, and END_VISUALPROPERTY_REGISTRATION
	somewhere in .cpp file (commonly immediately before/after the block containing the construction of visual properties), e.g.

	\code{.cpp}
	BEGIN_VISUALPROPERTY_REGISTRATION(MyVisualObject)
		REGISTER_VISUALPROPERTY(AnnotationEnabled);
		REGISTER_VISUALPROPERTY(AnnotationColor);
	END_VISUALPROPERTY_REGISTRATION(MyVisualObject)
	\endcode

	*/
	class vtkVisualPropertyRegistrator final
	{	
	private:
		vtkVisualPropertyRegistrator()
		{

		}
		
		~vtkVisualPropertyRegistrator();
	
	public:
		/** Gets the singleton registration authority for the visual property registration. */
		static const vtkVisualPropertyRegistrator* GetVisualPropertyRegistrator();

		/**
		Performs the registration of a visual property defined in the class identified by class_id and being derived 
		from the class identified by superclass_id. THIS METHOD IS THREAD SAFE.

		N.B. The method is typically not called directly but via the use of BEGIN_VISUALPROPERTY_REGISTRATION
		REGISTER_VISUALPROPERTY, and END_VISUALPROPERTY_REGISTRATION macros
		*/
		static void RegisterVisualProperty(std::type_index class_id, std::type_index superclass_id, vtkVisualPropertyKey* key);

		/** Finds the key by its global index in the entire collection.
		Returns NULL, if no such key exists. */
		const vtkVisualPropertyKey* FindKey(int GlobalIndex) const;

		/** Finds the key by its name in the entire collection.
		Returns NULL, if no such key exists.
		IMPORTANT NOTE: while GlobalIndex is unique, name may NOT */
		const vtkVisualPropertyKey* FindKey(const char* name) const;

		/** Finds the key by its global index starting from the specified class and optionally going to their superclasses.
		Returns NULL, if no such key exists. */
		const vtkVisualPropertyKey* FindKey(int GlobalIndex, std::type_index class_id, bool useInheritance = true) const;
		
		/** Finds the key by its name starting from the specified class and optionally going to their superclasses.
		Returns NULL, if no such key exists. */
		const vtkVisualPropertyKey* FindKey(const char* name, std::type_index class_id, bool useInheritance = true) const;


	private:
		std::unordered_map<std::type_index, vtkVisualPropertyClassSet*> m_RegisteredVisualProperties;	//!< Hierarchical table of the visual properties for every visual object class		
	};

#ifdef SHARED_EXPORTS_BUILT_AS_STATIC
	typedef vtkVisualPropertyRegistrator vtkVisualPropertyRegistrator2;
#else
	class vtkVisDbg_EXPORTS vtkVisualPropertyRegistrator2
	{
	public:
		static void RegisterVisualProperty(std::type_index class_id, std::type_index superclass_id, vtkVisualPropertyKey* key)
		{
			vtkVisualPropertyRegistrator::RegisterVisualProperty(class_id, superclass_id, key);
		}
	};
#endif

#define CONSTRUCT_VISUALPROPERTY(Class, Property, ...)	\
	/*static*/ decltype(Class::Property) Class::Property(#Property, __VA_ARGS__)

	/**
	This macro creates a static registrator for the particular class
	It is supposed to be in .CPP, .CXX.
	*/
#define BEGIN_VISUALPROPERTY_REGISTRATION(Class)	\
	namespace details {	\
		class Class##Registrator { \
			using ClassType = Class;	\
			using SuperClassType = Class::Superclass;	\
			public:	\
				Class##Registrator() {					

#define REGISTER_VISUALPROPERTY(Class, Property)	\
					vtkVisualPropertyRegistrator2::RegisterVisualProperty(typeid(ClassType), typeid(SuperClassType), &ClassType::Property)					 

#define END_VISUALPROPERTY_REGISTRATION(Class)	\
				}	\
		};	\
		Class##Registrator g_##Class##Registrator;	\
	}
		
}