/*

README:

- data na vstupu jsou vzdy zkonvertovana do vtkPolyData (vyjimkou jsou Labels, Axis a LocalFrame)
- vtkPolyData lze zobrazit ruznymi visualizacnimi technikami:
	- Points => points are represented by glyphs (of various shapes, sizes, ...)
	- Lines => Lines (or Polys, if Lines are not presented) are represented by tubes (of some colour, radius, ...)
	- Surface => 4 modes: POINTS, WIREFRAME, SURFACE, SURFACE + EDGES
	- ScalarField =>
- kazda vizualizacni technika je reprezentovana samostatnou tridou (oddedenou od spolecneho predka)
- ma tzv. visual properties prostrednictvim kterych se nastavuje vzhled z GUI, tj. visual property musi mit
	a) KEY = id, name, description, type, metadata = {clamping: min-max, allowed values, default value, ...}		
	b) VALUE = aktualni (efektivni) hodnota + callbacks metody pro nastaveni/precteni VTK hodnoty, tyhle metody nemohou byt staticke!
	vse se vytvari v konstruktoru a rusi automaticky v destruktoru (pres vtkSmartPointer)
- VisualProperty je 




*/

#include <typeindex>
#include <typeinfo>
#include <functional>
#include "vtkObject.h"
#include "vtkSetGet.h"


enum class VisualProperyDataTypes
{
	Unknown = -1,
	Int,
	Double,
	Vec3_Int,
	String,
};


template<typename T>
struct VisualProperyDataType
{
	using value_type = T;

};


struct Value
{
	

};

template< typename T >
struct ValueType : Value
{
	T value;

	ValueType(const T& v) : value(v)
	{

	}
};

class DependencyProperty
{
public:
	const char* Name;

	DependencyProperty(const char* name) : 
		Name(name)
	{

	}
};

template<typename T>
class DependencyPropertyT : public DependencyProperty
{
public:
	using value_type = T;

	DependencyPropertyT(const char* name) :
		DependencyProperty(name)
	{

	}
};


class DependencyPropertyValue
{

};

template<typename T>
class DependencyPropertyValueT
{
	T value;
};

class vtkClass : public vtkObject
{
public:
	vtkGetMacro(Visible, bool);
	vtkSetMacro(Visible, bool);

	vtkGetVector3Macro(Coords, double);
	vtkSetVector3Macro(Coords, double);

private:
	bool Visible;
	double* Coords;
};


class DependencyObject 
{
public:

	template<typename T>
	void SetPropertyValue(const DependencyProperty& dp, const T& value)
	{

	}

private:


};

class MyDO : public DependencyObject
{
public:
	MyDO()
	{

	}

public:
	static DependencyPropertyT<bool> Visible;

private:
	vtkClass* Impl;
};

DependencyPropertyT<bool> MyDO::Visible("Visible");

void test()
{
	Value** values = new Value*[5];
	values[0] = new ValueType<int>(5);
	values[1] = new ValueType<double>(5.2);
	values[2] = new ValueType<int*>(nullptr);



}