#include "vtkVisualObjectInternal.h"
#include "../Model/vtkVisualObject.h"

namespace vtkVisDbg
{
	//-------------------------------------------------------------------------
	// Finds the parent in the collection of parents
	size_t vtkVisualObjectInternal::FindParent(const vtkVisualObject* parent) const
	{
		size_t sz = m_Parents.size();
		for (size_t i = 0; i < sz; i++)
		{
			if (m_Parents[i] == parent)
				return i;
		}

		return -1;
	}	

	//-------------------------------------------------------------------------	
	// Gets property value inherited from the top visual object	
	const vtkVisualPropertyValue* vtkVisualObjectInternal::GetPropertyInheritedValue(const vtkVisualPropertyKey& prop) const
	{
		for (auto parent : this->m_Parents)
		{
			auto retVal = parent->GetVisualPropertyValue(prop);
			if (retVal != nullptr)
			{
				return retVal;
			}
		}
		
		return nullptr;
	}

	//-------------------------------------------------------------------------	
	// Gets the effective value of the properties,
	// @param	prop	The property whose value is to retrieve.	
	// otherwise it is left unset.
	//
	// If the effective value does not exist, it is created, otherwise the existing one is returned.
	vtkVisualPropertyValue* vtkVisualObjectInternal::CreatePropertyEffectiveValue(const vtkVisualPropertyKey& prop)
	{
		auto it = m_EffectiveProperties.find(prop.GetGlobalIndex());
		if (it != m_EffectiveProperties.end()) {
			return it->second;
		}

		vtkVisualPropertyValue* value = prop.CreatePropertyValue();
		m_EffectiveProperties[prop.GetGlobalIndex()] = value;		
		return value;
	}
}