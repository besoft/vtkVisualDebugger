#include "vtkVisualPropertyRegistrator.h"
#include <shared_mutex>

namespace vtkVisDbg
{
	//Meyer's Singleton for the registration
	/*static*/ const vtkVisualPropertyRegistrator* vtkVisualPropertyRegistrator::GetVisualPropertyRegistrator()
	{
		static vtkVisualPropertyRegistrator m_RegSingleton;
		return &m_RegSingleton;
	}

	//mutex to prevent concurrency
	std::shared_timed_mutex& GetRegistratorMutex()
	{
		static std::shared_timed_mutex g_RegistratorMutex;
		return g_RegistratorMutex;
	}

	vtkVisualPropertyRegistrator::~vtkVisualPropertyRegistrator()
	{
		const std::lock_guard<std::shared_timed_mutex> lock_wr(GetRegistratorMutex());	//lock (though this should be thread safe by principle)
		
		for (auto it : m_RegisteredVisualProperties) {
			delete it.second;
		}

		m_RegisteredVisualProperties.clear();
	}

	//-------------------------------------------------------------------------
	/*static*/ void vtkVisualPropertyRegistrator::RegisterVisualProperty(std::type_index class_id, std::type_index superclass_id, vtkVisualPropertyKey* key)
	{		
		auto& g_RegistratorMutex = GetRegistratorMutex();
		const std::lock_guard<std::shared_timed_mutex> lock_wr(g_RegistratorMutex);	//lock

		auto& map = const_cast<vtkVisualPropertyRegistrator*>(GetVisualPropertyRegistrator())->m_RegisteredVisualProperties;
		vtkVisualPropertyClassSet* pcs = nullptr;

		auto it = map.find(class_id);
		if (it != map.end())
			pcs = it->second;
		else
		{
			pcs = new vtkVisualPropertyClassSet(class_id, superclass_id);
			map[class_id] = pcs;
		}

		pcs->AddKey(key);
	}

	//-------------------------------------------------------------------------
	// Finds the key by its global index in the entire collection.
	// Returns NULL, if no such key exists.
	const vtkVisualPropertyKey* vtkVisualPropertyRegistrator::FindKey(int GlobalIndex) const
	{
		const std::shared_lock<std::shared_timed_mutex> lock_rd(GetRegistratorMutex());	//lock for reading (multiple readers are allowed)
		for (auto& it : m_RegisteredVisualProperties) 
		{
			auto ret = it.second->FindKey(GlobalIndex);
			if (ret != nullptr) {
				return ret;
			}
		}

		return nullptr;
	}

	//-------------------------------------------------------------------------
	// Finds the key by its name in the entire collection.
	//Returns NULL, if no such key exists.
	//IMPORTANT NOTE: while GlobalIndex is unique, name may NOT
	const vtkVisualPropertyKey* vtkVisualPropertyRegistrator::FindKey(const char* name) const
	{
		const std::shared_lock<std::shared_timed_mutex> lock_rd(GetRegistratorMutex());	//lock for reading (multiple readers are allowed)
		for (auto& it : m_RegisteredVisualProperties)
		{
			auto ret = it.second->FindKey(name);
			if (ret != nullptr) {
				return ret;
			}
		}
		return nullptr;
	}

	//-------------------------------------------------------------------------
	// Finds the key by its global index starting from the specified class and optionally going to their superclasses.
	//Returns NULL, if no such key exists.
	const vtkVisualPropertyKey* vtkVisualPropertyRegistrator::FindKey(int GlobalIndex, std::type_index class_id, bool useInheritance) const
	{
		const std::shared_lock<std::shared_timed_mutex> lock_rd(GetRegistratorMutex());	//lock for reading (multiple readers are allowed)
		
		while (true)
		{
			auto it = m_RegisteredVisualProperties.find(class_id);
			if (it == m_RegisteredVisualProperties.end()) {
				break;	//error, class not found
			}

			auto ret = it->second->FindKey(GlobalIndex);
			if (ret != nullptr) {
				return ret;
			}

			if (!useInheritance || !it->second->HasSuperClassType()) {
				break;	//error, property not found in this class and we cannot continue
			}

			class_id = it->second->GetSuperClassType();
		}

		return nullptr;
	}

	//-------------------------------------------------------------------------
	// Finds the key by its name starting from the specified class and optionally going to their superclasses.
	// Returns NULL, if no such key exists. 
	const vtkVisualPropertyKey* vtkVisualPropertyRegistrator::FindKey(const char* name, std::type_index class_id, bool useInheritance) const
	{
		const std::shared_lock<std::shared_timed_mutex> lock_rd(GetRegistratorMutex());	//lock for reading (multiple readers are allowed)

		while (true)
		{
			auto it = m_RegisteredVisualProperties.find(class_id);
			if (it == m_RegisteredVisualProperties.end()) {
				break;	//error, class not found
			}

			auto ret = it->second->FindKey(name);
			if (ret != nullptr) {
				return ret;
			}

			if (!useInheritance || !it->second->HasSuperClassType()) {
				break;	//error, property not found in this class and we cannot continue
			}

			class_id = it->second->GetSuperClassType();
		}

		return nullptr;
	}
}