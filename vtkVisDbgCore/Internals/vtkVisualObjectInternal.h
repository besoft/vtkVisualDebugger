#pragma once

#include <vector>
#include <string>
#include <unordered_map>
#include "vtkVisualPropertyInternal.h"

namespace vtkVisDbg
{
	class vtkVisualObject;
	class vtkVisualObjectInternal final
	{
	protected:
		vtkVisualObjectInternal() = default;
		~vtkVisualObjectInternal() = default;
	protected:
		/**
		Adds a new parent
		*/
		inline void AddParent(vtkVisualObject* parent)
		{
			const size_t index = FindParent(parent);
			if (index == static_cast<size_t>(-1))
			{
				m_Parents.push_back(parent);
			}
		}

		/**
		Removes the parent
		@param [in,out]	parent	If non-null, the parent.
		*/
		inline void RemoveParent(vtkVisualObject* parent)
		{
			RemoveParent(FindParent(parent));
		}

		/**
		Removes the parent described by index
		@param	index	Zero-based index of the parent to remove.
		*/
		inline void RemoveParent(size_t index)
		{
			m_Parents.erase(m_Parents.cbegin() + index);
		}

		/**
		Finds the parent in the collection of parents
		@param	parent	The parent to be found.
		@return	The index of the found parent, (size_t)-1 otherwise.
		*/
		size_t FindParent(const vtkVisualObject* parent) const;

		/**
		Gets the effective value of the property
		@param	prop	The property whose value is to retrieve.
		@return	Null if it fails, i.e., no effective value is specified.
		*/
		inline const vtkVisualPropertyValue* GetPropertyEffectiveValue(const vtkVisualPropertyKey& prop) const
		{
			auto it = m_EffectiveProperties.find(prop.GetGlobalIndex());
			if (it != m_EffectiveProperties.end())
				return it->second;

			return nullptr;	//no effective value available
		}

		/**
		Gets the effective value of the properties,
		@param	prop	The property whose value is to retrieve.
		
		If the effective value does not exist, it is created (unset), otherwise the existing one is returned.
		*/
		vtkVisualPropertyValue* CreatePropertyEffectiveValue(const vtkVisualPropertyKey& prop);

		/**
		Gets property value inherited from the top visual object
		@param	prop	The property whose value is to retrieve.
		@return	Null if it fails, else the property inherited value.
		*/
		const vtkVisualPropertyValue* GetPropertyInheritedValue(const vtkVisualPropertyKey& prop) const;

	protected:
		std::string m_Name; //!< The name of the visual object

		std::vector<vtkVisualObject*> m_Parents;	//!< Contains all the parent visual objects in which this visual object participates
		std::vector<vtkVisualObject*> m_Children;	//!< Contains all the visual objects having this visual object as an parent

		std::unordered_map<int, vtkVisualPropertyValue*> m_EffectiveProperties; //!< Contains the effective values of the properties for this visual object
		friend class vtkVisualObject;
	};
}
