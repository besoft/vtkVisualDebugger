#pragma once

#include "Model/vtkVisualObject.h"

class vtkObject;
class vtkProp;

class vtkVisualObject_Old //: public vtkVisDbg::vtkVisualObject
{
public:
	enum class DataKind
	{
		Unknown = -1,	///<this is external/unknown visual representation
		LocalFrame,
		Axis,
		Points,
		Lines,
		Surface,
		ScalarField,
		Label,
		PickAnnotation,
	};

public:
	const static int MAX_ACTORS = 4;

	DataKind Kind;

	vtkObject* pResizableSource;				///<item having resizable values 
	vtkProp* pActors[MAX_ACTORS];				///<list of actors

	vtkVisualObject_Old() {
		Kind = DataKind::Unknown;
		pResizableSource = nullptr;
		for (int i = 0; i < MAX_ACTORS; i++) {
			pActors[i] = nullptr;
		}
	}	
};

class vtkVisualLocalFrame : public vtkVisualObject_Old
{
public:
	vtkVisualLocalFrame() {
		Kind = DataKind::LocalFrame;
	}
};

class vtkVisualAxis : public vtkVisualObject_Old
{
public:
	vtkVisualAxis() {
		Kind = DataKind::Axis;
	}
};

class vtkVisualPoints : public vtkVisualObject_Old
{
public:
	vtkVisualPoints() {
		Kind = DataKind::Points;
	}
};

class vtkVisualLines : public vtkVisualObject_Old
{
public:
	vtkVisualLines() {
		Kind = DataKind::Lines;
	}
};

class vtkVisualSurface : public vtkVisualObject_Old
{
public:
	vtkVisualSurface() {
		Kind = DataKind::Surface;
	}
};

class vtkVisualScalarField : public vtkVisualObject_Old
{
public:
	vtkVisualScalarField() {
		Kind = DataKind::ScalarField;
	}
};

class vtkVisualLabel : public vtkVisualObject_Old
{
public:
	vtkVisualLabel() {
		Kind = DataKind::Label;
	}
};

class vtkVisualPickAnnotation : public vtkVisualObject_Old
{
public:
	vtkVisualPickAnnotation() {
		Kind = DataKind::PickAnnotation;
	}
};