#pragma once

#if defined(_WIN32) || defined(WIN32) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)
#define OS_WIN
#endif

#ifndef OS_WIN
#define sprintf_s snprintf
#define fopen_s(pFile,filename,mode) ((*(pFile))=fopen((filename),(mode)))
#endif

#include "vtkVisualObject_Old.h"
#include <unordered_map>

class vtkMAFVisualDebuggerInternal final
{
protected:
	vtkMAFVisualDebuggerInternal() = default;
	~vtkMAFVisualDebuggerInternal() = default;

protected:
	typedef std::unordered_map< std::string, vtkVisualObject_Old*  > VisualObjectsMap;
	VisualObjectsMap m_VisualObjects;	///<list of objects to be visualized

	friend class vtkMAFVisualDebugger;
};
