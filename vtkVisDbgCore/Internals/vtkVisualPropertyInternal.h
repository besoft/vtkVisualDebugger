#pragma once

#include "../vtkVisDbgDefs.h"
#include "../Model/vtkVisualProperty.h"

#include <typeindex>
#include <vector>

namespace vtkVisDbg
{
	class vtkVisualPropertyClassSet final
	{
	private:
		std::type_index m_ThisClass;
		std::type_index m_SuperClass;
		std::vector<vtkVisualPropertyKey*> m_Keys;

	public:
		vtkVisualPropertyClassSet(std::type_index class_id, std::type_index superclass_id) :
			m_ThisClass(class_id), m_SuperClass(superclass_id)
		{

		}

		~vtkVisualPropertyClassSet() {
			RemoveAllKeys();
		}

	public:
		/** Adds the key at the proper place in Keys collection */
		void AddKey(vtkVisualPropertyKey* key);

		/** Finds the key by its global index.
		Returns NULL, if no such key exists. */
		vtkVisualPropertyKey* FindKey(int GlobalIndex);
		
		/** Finds the key by its name.
		Returns NULL, if no such key exists. */
		vtkVisualPropertyKey* FindKey(const char* name);

		/** Removes all keys in the collection. */
		void RemoveAllKeys();

		/** Gets the type information for the class that registered the keys. */
		std::type_index GetClassType() {
			return m_ThisClass;
		}

		/** Gets the type information for the base class from which the 
		class that registered the keys is derived. */
		std::type_index GetSuperClassType() {
			return m_SuperClass;
		}

		/** Returns false, if the class is not derived from any other supported class. 
		N.B. should be false only for vtkVisualObject class. */
		bool HasSuperClassType()
		{
			return std::type_index(typeid(void)) == m_SuperClass;
		}

		/** Returns number of keys. */
		auto GetNumberOfKeys() {
			return m_Keys.size();
		}

		/** Returns the key at the specified index. */
		const vtkVisualPropertyKey* GetKey(int index) {
			return m_Keys[index];
		}		
	};
}
