#include "vtkVisualPropertyInternal.h"
#include <algorithm>
#include <cstring>

namespace vtkVisDbg
{
	void vtkVisualPropertyClassSet::RemoveAllKeys()
	{
		//Keys are static and therefore deleted automatically

		m_Keys.clear();
	}

	void vtkVisualPropertyClassSet::AddKey(vtkVisualPropertyKey* key)
	{
		//add the key so that the keys are ordered by their global indices
		auto it = std::lower_bound(m_Keys.begin(), m_Keys.end(), key, 
			[](const vtkVisualPropertyKey* x1, const vtkVisualPropertyKey* x2)
			{
				return x1->GetGlobalIndex() < x2->GetGlobalIndex();
			}
			);

		m_Keys.insert(it, key);
	}


	vtkVisualPropertyKey* vtkVisualPropertyClassSet::FindKey(int GlobalIndex)
	{
		//use binary search since the keys are ordered by their GlobalIndex
		auto it = std::lower_bound(m_Keys.begin(), m_Keys.end(), GlobalIndex,
			[](const vtkVisualPropertyKey* x1, int index)
			{
				return x1->GetGlobalIndex() < index;
			}
		);

		if (it == m_Keys.cend()) {
			return nullptr;
		}

		return *it;
	}

	vtkVisualPropertyKey* vtkVisualPropertyClassSet::FindKey(const char* name)
	{
		//use sequential search
		auto predicate = [=](const vtkVisualPropertyKey* x)
		{
			return 0 == strcmp(x->GetName(), name);
		};

		auto it = std::find_if(m_Keys.cbegin(), m_Keys.cend(), predicate);
		if (it == m_Keys.cend()) {
			return nullptr;
		}

		return *it;
	}
}
