#include "vtkPolyData.h"
#include "vtkCellArray.h"
#include "vtkPoints.h"
#include "vtkSmartPointer.h"
#include "vtkTextMapper.h"
#include "vtkTextActor.h"
#include "vtkProperty2D.h"
#include "vtkTextProperty.h"

#include "vtkVisualDebugger.h"

int main(int argc, char** argv)
{
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	points->InsertNextPoint(0, 0, 0);
	points->InsertNextPoint(0, 0, 1);
	points->InsertNextPoint(2, 3, 4);
	points->InsertNextPoint(5, 6, 7);	

	vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
	cells->InsertNextCell(3);
	cells->InsertCellPoint(0);
	cells->InsertCellPoint(1);
	cells->InsertCellPoint(2);
	cells->InsertNextCell(2);
	cells->InsertCellPoint(3);
	cells->InsertCellPoint(0);

	vtkSmartPointer<vtkPolyData> poly = vtkSmartPointer<vtkPolyData>::New();
	poly->SetPoints(points);
	poly->SetPolys(cells);

	vtkVisDbg::vtkVisualDebugger* db = vtkVisDbg::vtkVisualDebugger::New();
	db->AddOrUpdatePoints("POINTS", poly, 1, 1, 0, 0);
	db->EnablePickable("POINTS");
	
	db->AddOrUpdateLabel("__HELPMSG", "TEST", 12,
		0.5, 0.5, 0.5,	//RED
		0.05, 0.85,		//Center position
		-1, -1);		//centered in x and y


	double x = 0.5, y = 0.5;
	int horiz_align = -1;
	int vert_align = -1;




	vtkSmartPointer< vtkTextMapper > textMapper = vtkTextMapper::New();
	textMapper->UnRegister(nullptr);
	//textMapper->SetInput("Bla bla bla");

	vtkSmartPointer < vtkTextActor > textActor = vtkTextActor::New();
	textActor->UnRegister(nullptr);
	//textActor->SetMapper(textMapper.GetPointer());
	textActor->SetInput("Toto je text pro zobrazeni, ktery je velmi\nale opravdu vemi dlouhy. Ma nekolik\nradek a uz nevim, co psat.\n agds gds gg");

	//calculate position
	double delta_x = 0.0, delta_y = 0.0;
	if (horiz_align == 0)
		delta_x = 0.5;
	else if (horiz_align > 0)
		delta_x = 1.0;

	if (vert_align == 0)
		delta_y = 0.5;
	else if (vert_align > 0)
		delta_y = 1.0;

	textActor->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
	textActor->GetPositionCoordinate()->SetValue(x + delta_x, y + delta_y);
	textActor->GetProperty()->SetColor(0.5, 0.5, 0.5);

	textActor->GetTextProperty()->SetFontSize(48);
	textActor->GetTextProperty()->SetJustificationToLeft();
	textActor->GetTextProperty()->SetVerticalJustificationToCentered();


	db->AddOrUpdateExternalObject("PPP", textActor);

	
	db->DebugStep();


	/*auto root = vtkVisDbg::vtkVisualObject::New();

	auto vo1 = vtkVisDbg::vtkVisualObject::New();
	auto vo2 = vtkVisDbg::vtkVisualObject::New();

	root->AddChild(vo1);
	root->AddChild(vo2);

	vo1->Delete();
	vo2->Delete();

	vo1->AddChild(vo2);
	root->RemoveChild(vo1);
	
	root->Delete();*/

	return 0;
}
